﻿#include <iostream>
#include <fstream>
#include <direct.h>
#include "lzs.h"
#include "Map.h"
#include "StartData.h"
#include "../shared/DataStructure.h"
#include "../shared/AnimationStructure.h"
#include "Animation.h"

bool CreateMissingFolders() {
    if (_mkdir("../extracted") != 0 && errno != 17) {
        printf("Error creating directory '../extracted'!\n");
        return false;
    }
    if (_mkdir("../extracted/txt") != 0 && errno != 17) {
        printf("Error creating directory '../extracted/txt'!\n");
        return false;
    }
    if (_mkdir("../extracted/data") != 0 && errno != 17) {
        printf("Error creating directory '../extracted/data'!\n");
        return false;
    }
    if (_mkdir("../extracted/data/mappack") != 0 && errno != 17) {
        printf("Error creating directory '../extracted/data/mappack'!\n");
        return false;
    }
    if (_mkdir("../extracted/sub_data") != 0 && errno != 17) {
        printf("Error creating directory '../extracted/sub_data'!\n");
        return false;
    }
    if (_mkdir("../extracted/sub_data/anmpack") != 0 && errno != 17) {
        printf("Error creating directory '../extracted/sub_data/anmpack'!\n");
        return false;
    }
    if (_mkdir("../extracted/sub_data/anmpack/small_raw") != 0 && errno != 17) {
        printf("Error creating directory '../extracted/sub_data/anmpack/small_raw'!\n");
        return false;
    }
    if (_mkdir("../extracted/sub_data/txppack_pc") != 0 && errno != 17) {
        printf("Error creating directory '../extracted/sub_data/txppack_pc'!\n");
        return false;
    }
    if (_mkdir("../extracted/sub_data/uitxp") != 0 && errno != 17) {
        printf("Error creating directory '../extracted/sub_data/uitxp'!\n");
        return false;
    }
    if (_mkdir("../extracted/sub_data/start") != 0 && errno != 17) {
        printf("Error creating directory '../extracted/sub_data/start'!\n");
        return false;
    }
    if (_mkdir("../extracted/sub_data/start/small_raw") != 0 && errno != 17) {
        printf("Error creating directory '../extracted/sub_data/start/small_raw'!\n");
        return false;
    }
    if (_mkdir("../extracted/sub_data/start_vm") != 0 && errno != 17) {
        printf("Error creating directory '../extracted/sub_data/start_vm'!\n");
        return false;
    }
    if (_mkdir("../extracted/sub_data/start_vm/small_raw") != 0 && errno != 17) {
        printf("Error creating directory '../extracted/sub_data/start_vm/small_raw'!\n");
        return false;
    }
    return true;
}

bool ExtractData(const char* inFileName, const char* outFileDir) {
    FILE *dataFile, *outFile;
    if (0 < fopen_s(&dataFile, inFileName, "rb") || dataFile == nullptr) {
        printf("Error opening %s for input!\n", inFileName);
        return false;
    }

    DataHeader header;
    fread((void *)&header.version, 1, 8, dataFile);
    fread((void *)&header.entryCount, 1, 4, dataFile);
    fread((void *)&header.empty, 1, 4, dataFile);
    header.entries = new DataHeaderEntry[header.entryCount];

    std::string indexFileName(outFileDir);
    indexFileName.append("index.txt");
    std::ofstream indexFile(indexFileName);
    if (!indexFile.is_open()) {
        printf("Error opening %s for output!\n", indexFileName.c_str());
        return false;
    }
    indexFile << header.entryCount << "\n";
    for (uint32_t i = 0; i < header.entryCount; i++) {
        fread((void *)&header.entries[i], 1, 52, dataFile);
        indexFile << header.entries[i].name << " " << header.entries[i].unk << "\n";
        std::cout << header.entries[i].name << " " << header.entries[i].unk << " " << header.entries[i].size << " " << header.entries[i].start << std::endl;
    }
    indexFile.close();
    for (uint32_t i = 0; i < header.entryCount; i++) {
        std::string outFileName(outFileDir);
        outFileName.append(header.entries[i].name);
        if (0 < fopen_s(&outFile, outFileName.c_str(), "wb") || outFile == nullptr) {
            printf("Error opening %s for output!\n", outFileName.c_str());
            return false;
        }
        fseek(dataFile, header.entries[i].start, SEEK_SET);
        char* content = new char[header.entries[i].size];
        fread(content, 1, header.entries[i].size, dataFile);
        fwrite(content, 1, header.entries[i].size, outFile);
        fclose(outFile);
        delete[] content;
    }
    fclose(dataFile);
    delete[] header.entries;
    return true;
}

bool ExtractDataFile(const char* inFileName, const char* outFileDir) {
    FILE *dataFile, *outFile;
    if (0 < fopen_s(&dataFile, inFileName, "rb") || dataFile == nullptr) {
        printf("Error opening %s for input!\n", inFileName);
        return false;
    }

    fseek(dataFile, 0, SEEK_END);
    size_t filesize = ftell(dataFile);
    fseek(dataFile, 0, SEEK_SET);
    if (filesize < 16) {
        fclose(dataFile);
        return true;
    }

    StartDataHeader header;
    fread((void*)&header.entryCount, 1, 4, dataFile);
    fread((void*)&header.empty, 1, 12, dataFile);
    header.entries = new StartDataHeaderEntry[header.entryCount];

    std::string indexFileName(outFileDir);
    indexFileName.append("index.txt");
    std::ofstream indexFile(indexFileName);
    if (!indexFile.is_open()) {
        printf("Error opening %s for output!\n", indexFileName.c_str());
        return false;
    }
    indexFile << header.entryCount << "\n";
    for (uint32_t i = 0; i < header.entryCount; i++) {
        fread((void*)&header.entries[i], 1, 32, dataFile);
        indexFile << header.entries[i].name << "\n";
        printf("%s %x\n", header.entries[i].name, header.entries[i].end);
    }
    indexFile.close();

    for (uint32_t i = 0; i < header.entryCount; i++) {
        std::string outFileName(outFileDir);
        outFileName.append(header.entries[i].name);
        if (0 < fopen_s(&outFile, outFileName.c_str(), "wb") || outFile == nullptr) {
            printf("Error opening %s for output!\n", outFileName.c_str());
            return false;
        }
        size_t pos = ftell(dataFile);
        const size_t size = header.entries[i].end - pos + header.entryCount * 32 + 16;
        char* content = new char[size];
        fread(content, 1, size, dataFile);
        fwrite(content, 1, size, outFile);
        fclose(outFile);
        delete[] content;
    }
    fclose(dataFile);
    delete[] header.entries;
    return true;
}

bool ExtractMappack(const char* outFileDir, NispackHeader* header) {
    for (uint32_t i = 0; i < header->entryCount; i++) {
        std::string outFileName(header->entries[i].name);
        if (outFileName.substr(outFileName.length() - 3) == "lzs") {
            outFileName.resize(outFileName.length() - 4);

            std::string lzsFileName(outFileDir);
            lzsFileName.append(header->entries[i].name);
            std::string datFileDir(outFileDir);
            datFileDir.append(outFileName).append("/");
            if (_mkdir(datFileDir.c_str()) != 0 && errno != 17) {
                printf("Error creating directory '%s'!\n", datFileDir.c_str());
                return false;
            }

            std::string datFileName(datFileDir);
            datFileName.append(outFileName).append(".dat");
            if (!DecompressData(lzsFileName.c_str(), datFileName.c_str()))
                return false;
            if (!ExtractDataFile(datFileName.c_str(), datFileDir.c_str()))
                return false;

            if (outFileName == "mp01315" || outFileName == "mp01316" || outFileName == "mp01704" || outFileName == "mp06601" || outFileName == "mp06701" ||
                outFileName == "mp06801" || outFileName == "mp07001" || outFileName == "mp12001" || outFileName == "mp21001" || outFileName == "mp30004") {
                //ignore the following maps as those have some strange data
                //01315 DW Final Decision
                //01316 DW Viewing Room
                //01704 The Dark Hero!
                //06601 For Axel Demo
                //06701 Axel's Home
                //06801 Axel's Base
                //07001 EMPTY 
                //12001 Dark Court
                //21001 Local Assembly
                //30004 Arena Room
                continue;
            }

            std::string chrFileName(datFileDir);
            chrFileName.append(outFileName).append(".chr");
            if (!AppendToMapChr(chrFileName.c_str(), i == 0))
                return false;

            std::string posFileName(datFileDir);
            posFileName.append(outFileName).append(".pos");
            if (!AppendToMapPos(posFileName.c_str(), i == 0))
                return false;
        }
    }
    return true;
}

bool ExtractNispack(const char* inFileName, const char* outFileDir, bool isMappack) {
    FILE* dataFile, * outFile;
    if (0 < fopen_s(&dataFile, inFileName, "rb") || dataFile == nullptr) {
        printf("Error opening %s for input!\n", inFileName);
        return false;
    }

    NispackHeader header;
    fread((void*)&header.version, 1, 8, dataFile);
    fread((void*)&header.empty, 1, 4, dataFile);
    fread((void*)&header.entryCount, 1, 4, dataFile);
    header.entries = new NispackHeaderEntry[header.entryCount];

    std::string indexFileName(outFileDir);
    indexFileName.append("index.txt");
    std::ofstream indexFile(indexFileName);
    if (!indexFile.is_open()) {
        printf("Error opening %s for output!\n", indexFileName.c_str());
        return false;
    }
    indexFile << header.entryCount << "\n";
    for (uint32_t i = 0; i < header.entryCount; i++) {
        fread((void*)&header.entries[i], 1, 44, dataFile);
        indexFile << header.entries[i].name << " " << header.entries[i].timestamp << "\n";
        std::cout << header.entries[i].name << " " << header.entries[i].timestamp << " " << header.entries[i].size << " " << header.entries[i].start << std::endl;
    }
    indexFile.close();
    for (uint32_t i = 0; i < header.entryCount; i++) {
        std::string outFileName(outFileDir);
        outFileName.append(header.entries[i].name);
        if (0 < fopen_s(&outFile, outFileName.c_str(), "wb") || outFile == nullptr) {
            printf("Error opening %s for output!\n", outFileName.c_str());
            return false;
        }
        fseek(dataFile, header.entries[i].start, SEEK_SET);
        char* content = new char[header.entries[i].size];
        fread(content, 1, header.entries[i].size, dataFile);
        fwrite(content, 1, header.entries[i].size, outFile);
        fclose(outFile);
        delete[] content;
    }
    fclose(dataFile);

    if (isMappack)
        ExtractMappack(outFileDir, &header);
    
    delete[] header.entries;
    return true;
}

bool ExtractDsarc(const char* inFileName, const char* outFileDir) {
    FILE* dataFile, * outFile;
    if (0 < fopen_s(&dataFile, inFileName, "rb") || dataFile == nullptr) {
        printf("Error opening %s for input!\n", inFileName);
        return false;
    }

    DsarcHeader header;
    fread((void*)&header.version, 1, 8, dataFile);
    fread((void*)&header.entryCount, 1, 4, dataFile);
    fread((void*)&header.empty, 1, 4, dataFile);
    header.entries = new DsarcHeaderEntry[header.entryCount];

    std::string indexFileName(outFileDir);
    indexFileName.append("index.txt");
    std::ofstream indexFile(indexFileName);
    if (!indexFile.is_open()) {
        printf("Error opening %s for output!\n", indexFileName.c_str());
        return false;
    }
    indexFile << header.entryCount << "\n";
    for (uint32_t i = 0; i < header.entryCount; i++) {
        fread((void*)&header.entries[i], 1, 48, dataFile);
        indexFile << header.entries[i].name << "\n";
        std::cout << header.entries[i].name << " " << header.entries[i].size << " " << header.entries[i].start << std::endl;
    }
    indexFile.close();
    for (uint32_t i = 0; i < header.entryCount; i++) {
        std::string outFileName(outFileDir);
        outFileName.append(header.entries[i].name);
        if (0 < fopen_s(&outFile, outFileName.c_str(), "wb") || outFile == nullptr) {
            printf("Error opening %s for output!\n", outFileName.c_str());
            return false;
        }
        fseek(dataFile, header.entries[i].start, SEEK_SET);
        char* content = new char[header.entries[i].size];
        fread(content, 1, header.entries[i].size, dataFile);
        fwrite(content, 1, header.entries[i].size, outFile);
        fclose(outFile);
        delete[] content;
    }
    fclose(dataFile);
    delete[] header.entries;
    return true;
}

bool ExtractAnimation() {
    if (useAnimation) {
        if (!ExtractAnimationNispack(std::filesystem::path("../extracted/sub_data/ANMPACK.DAT"), std::filesystem::path("../extracted/sub_data/anmpack/")))
            return false;
        if (!ConvertAnimationDataToHD(std::filesystem::path("../extracted/sub_data/start/anm0000.dat")))
            return false;
        if (!ConvertAnimationDataToHD(std::filesystem::path("../extracted/sub_data/start_vm/anm0001.dat")))
            return false;
        if (!ConvertAnimationDataToHD(std::filesystem::path("../extracted/sub_data/start_vm/anm0002.dat")))
            return false;
        if (!ExtractAnimationData(std::filesystem::path("../extracted/sub_data/start/anm0000.dat")))
            return false;
        if (!ExtractAnimationData(std::filesystem::path("../extracted/sub_data/start_vm/anm0001.dat")))
            return false;
        if (!ExtractAnimationData(std::filesystem::path("../extracted/sub_data/start_vm/anm0002.dat")))
            return false;
    }
    return true;
}

int main() {
    if (!CreateMissingFolders()) 
        return 1;

    if (!ExtractData("../../SUB_D001.DAT", "../extracted/sub_data/"))
        return 1;
    if (!ExtractNispack("../extracted/sub_data/TXPPACK_PC.DAT", "../extracted/sub_data/txppack_pc/", false))
        return 1;
    if (!ExtractDsarc("../extracted/sub_data/UITXP.DAT", "../extracted/sub_data/uitxp/"))
        return 1;
    if (!DecompressData("../extracted/sub_data/start.lzs", "../extracted/sub_data/start.dat"))
        return 1;
    if (!DecompressData("../extracted/sub_data/start_vm.lzs", "../extracted/sub_data/start_vm.dat"))
        return 1;
    if (!ExtractDataFile("../extracted/sub_data/start.dat", "../extracted/sub_data/start/"))
        return 1;
    if (!ExtractDataFile("../extracted/sub_data/start_vm.dat", "../extracted/sub_data/start_vm/"))
        return 1;
    if (!ExtractAnimation())
        return 1;
    if (!MakeStartTxtFiles())
        return 1;

    if (!ExtractData("../../D001.DAT", "../extracted/data/"))
        return 1;
    if (!ExtractNispack("../extracted/data/MAPPACK.DAT", "../extracted/data/mappack/", true))
        return 1;
    printf("\nDone!");
    return 0;
}

