#include <iostream>
#include <fstream>
#include <iomanip>
#include "../shared/StartDataStructure.h"

bool MakeTxtChar() {
    FILE *inFile;

    if (0 < fopen_s(&inFile, "../extracted/sub_data/start/char.dat", "rb") || inFile == nullptr) {
        printf("Error opening ../extracted/sub_data/start/char.dat for input!\n");
        return false;
    }

    uint32_t recordCount;
    fread((void *)&recordCount, 4, 1, inFile);
    
    character* data = new character[recordCount];
    for (uint32_t i = 0; i < recordCount; i++) {
        if (sizeof(character) != fread((void *)&data[i], 1, sizeof(character), inFile)) {
            printf("Error reading ../extracted/sub_data/start/char.dat!\n");
            return false;
        }
    }

    const char* size =
        "52\t69\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t"
        "1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t"
        "2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t"
        "2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t"
        "2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t"
        "2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t"
        "2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t"
        "2\t15x2";
    const char* header =
        "name\ttitle\tgender\tspellCount\ttier\tunk1\tjumpHeight\tprofFists\tprofSword\tprofSpear\tprofBow\tprofGun\tprofAxe\tprofStaff\t"
        "unk2\tspecAbility\taptHP\taptSP\taptATK\taptDEF\taptINT\taptSPD\taptHIT\taptRES\tunk3\ttype\tthrowDist\tcounter\tunk4\tunk5\tunk6\t"
        "id\tfirst\tunk7\tunk8\tdesc\tstatHP\tstatSP\tstatATK\tstatDEF\tstatINT\tstatSPD\tstatHIT\tstatRES\texpRate\tmoveDist\tcanFly\tattackRange\tkillHell\tkillExp\tunk9\tcreateMana\t"
        "spellId1\tspellId2\tspellId3\tspellId4\tspellId5\tspellId6\tspellId7\tspellId8\tspellId9\tspellId10\tspellId11\tspellId12\tspellId13\tspellId14\tspellId15\tspellId16\t"
        "spellId17\tspellId18\tspellId19\tspellId20\tspellId21\tspellId22\tspellId23\tspellId24\tspellId25\tspellId26\tspellId27\tspellId28\tspellId29\tspellId30\tspellId31\tspellId32\t"
        "learnedAt1\tlearnedAt2\tlearnedAt3\tlearnedAt4\tlearnedAt5\tlearnedAt6\tlearnedAt7\tlearnedAt8\tlearnedAt9\tlearnedAt10\tlearnedAt11\tlearnedAt12\tlearnedAt13\tlearnedAt14\tlearnedAt15\tlearnedAt16\t"
        "learnedAt17\tlearnedAt18\tlearnedAt19\tlearnedAt20\tlearnedAt21\tlearnedAt22\tlearnedAt23\tlearnedAt24\tlearnedAt25\tlearnedAt26\tlearnedAt27\tlearnedAt28\tlearnedAt29\tlearnedAt30\tlearnedAt31\tlearnedAt32\t"
        "id2\tempty";

    std::ofstream outFile("../extracted/txt/char.txt");
    if (!outFile.is_open()) {
        printf("Error opening ../extracted/txt/char.txt for output!\n");
        return false;
    }
    
    outFile << size << "\n" << header << "\n";
    for (uint32_t i = 0; i < recordCount; i++) {
        outFile << data[i].name << "\t" << data[i].title << "\t" << (uint32_t)data[i].gender << "\t" << (uint32_t)data[i].spellCount << "\t" << (uint32_t)data[i].tier << "\t" << (uint32_t)data[i].unk1
            << "\t" << (uint32_t)data[i].jumpHeight << "\t" << (uint32_t)data[i].profFists << "\t" << (uint32_t)data[i].profSword << "\t" << (uint32_t)data[i].profSpear << "\t" << (uint32_t)data[i].profBow
            << "\t" << (uint32_t)data[i].profGun << "\t" << (uint32_t)data[i].profAxe << "\t" << (uint32_t)data[i].profStaff << "\t" << (uint32_t)data[i].unk2 << "\t" << (uint32_t)data[i].specAbility
            << "\t" << (uint32_t)data[i].aptHP << "\t" << (uint32_t)data[i].aptSP << "\t" << (uint32_t)data[i].aptATK << "\t" << (uint32_t)data[i].aptDEF << "\t" << (uint32_t)data[i].aptINT
            << "\t" << (uint32_t)data[i].aptSPD << "\t" << (uint32_t)data[i].aptHIT << "\t" << (uint32_t)data[i].aptRES << "\t" << (uint32_t)data[i].unk3 << "\t" << (uint32_t)data[i].type
            << "\t" << (uint32_t)data[i].throwDist << "\t" << (uint32_t)data[i].counter << "\t" << (uint32_t)data[i].unk4 << "\t" << (uint32_t)data[i].unk5 << "\t" << (uint32_t)data[i].unk6
            << "\t" << data[i].id << "\t" << data[i].first << "\t" << data[i].unk7 << "\t" << data[i].unk8 << "\t" << data[i].desc
            << "\t" << data[i].statHP << "\t" << data[i].statSP << "\t" << data[i].statATK << "\t" << data[i].statDEF << "\t" << data[i].statINT
            << "\t" << data[i].statSPD << "\t" << data[i].statHIT << "\t" << data[i].statRES << "\t" << data[i].expRate << "\t" << data[i].moveDist << "\t" << data[i].canFly
            << "\t" << data[i].attackRange << "\t" << data[i].killHell << "\t" << data[i].killExp << "\t" << data[i].unk9 << "\t" << data[i].createMana << "\t";
        for (uint32_t j = 0; j < 32; j++)
            outFile << data[i].spellId[j] << "\t";
        for (uint32_t j = 0; j < 32; j++)
            outFile << data[i].spellLearnedAt[j] << "\t";
        outFile << data[i].id2 << "\t";
        for (uint32_t j = 0; j < 15; j++)
            outFile << data[i].empty[j] << " ";
        outFile << "\n";
    }
    outFile.close();
    return true;
}

bool MakeTxtCharHelp() {
    FILE *inFile;

    if (0 < fopen_s(&inFile, "../extracted/sub_data/start/charhelp.dat", "rb") || inFile == nullptr) {
        printf("Error opening ../extracted/sub_data/start/charhelp.dat for input!\n");
        return false;
    }

    uint32_t recordCount;
    fread((void *)&recordCount, 4, 1, inFile);

    characterhelp* data = new characterhelp[recordCount];
    for (uint32_t i = 0; i < recordCount; i++) {
        if (sizeof(characterhelp) != fread((void *)&data[i], 1, sizeof(characterhelp), inFile)) {
            printf("Error reading ../extracted/sub_data/start/charhelp.dat!\n");
            return false;
        }
    }

    const char* size = "92\t92\t92\t4\t4\t4";
    const char* header = "line1\tline2\tline3\tid\tid2\tempty";

    std::ofstream outFile("../extracted/txt/charhelp.txt");
    if (!outFile.is_open()) {
        printf("Error opening ../extracted/txt/charhelp.txt for output!\n");
        return false;
    }

    outFile << size << "\n" << header << "\n";
    for (uint32_t i = 0; i < recordCount; i++)
        outFile << data[i].line1 << "\t" << data[i].line2 << "\t" << data[i].line3 << "\t" << data[i].id << "\t" << data[i].id2 << "\t" << data[i].empty << "\n";
    outFile.close();
    return true;
}

bool MakeTxtDungeon() {
    FILE *inFile;

    if (0 < fopen_s(&inFile, "../extracted/sub_data/start/dungeon.dat", "rb") || inFile == nullptr) {
        printf("Error opening ../extracted/sub_data/start/dungeon.dat for input!\n");
        return false;
    }

    uint32_t recordCount;
    fread((void *)&recordCount, 4, 1, inFile);

    dungeon* data = new dungeon[recordCount];
    for (uint32_t i = 0; i < recordCount; i++) {
        if (sizeof(dungeon) != fread((void *)&data[i], 1, sizeof(dungeon), inFile)) {
            printf("Error reading ../extracted/sub_data/start/dungeon.dat!\n");
            return false;
        }
    }

    const char* size = "52\t4\t4\t3x4\t2\t2\t2\t2\t1\t1\t1\t1\t1\t23x1\t2\t2\t4\t3x4";
    const char* header = "name\tid\tdwTarget\tempty\tunk1\tbonusRank\tdwCondition\tempty\tdwUnlock\tremoveUnique\tempty\tdwFlag\tremoveUnique\tempty\tdwMap\tdwLevel\tid2\tempty";

    std::ofstream outFile("../extracted/txt/dungeon.txt");
    if (!outFile.is_open()) {
        printf("Error opening ../extracted/txt/dungeon.txt for output!\n");
        return false;
    }

    outFile << size << "\n" << header << "\n";
    for (uint32_t i = 0; i < recordCount; i++) {
        outFile << data[i].name << "\t" << data[i].id << "\t" << data[i].dwTarget << "\t" << data[i].empty[0] << " " << data[i].empty[1] << " " << data[i].empty[2] << " \t" << 
            data[i].unk1 << "\t" << data[i].bonusRank << "\t" << data[i].dwCondition << "\t" << data[i].empty2 << "\t" << (uint32_t)data[i].dwUnlock << "\t" << 
            (uint32_t)data[i].removeUnique1 << "\t" << (uint32_t)data[i].empty3 << "\t" << (uint32_t)data[i].dwFlag << "\t" << (uint32_t)data[i].removeUnique2 << "\t";
        for (uint32_t j = 0; j < 23; j++)
            outFile << (uint32_t)data[i].empty4[j] << " ";
        outFile << "\t" << data[i].dwMap << "\t" << data[i].dwLevel << "\t" << data[i].id2 << "\t" << data[i].empty5[0] << " " << data[i].empty5[1] << " " << data[i].empty5[2] << " \n";
    }
    outFile.close();
    return true;
}

bool MakeTxtHabit() {
    FILE *inFile;

    if (0 < fopen_s(&inFile, "../extracted/sub_data/start/HABIT.dat", "rb") || inFile == nullptr) {
        printf("Error opening ../extracted/sub_data/start/HABIT.dat for input!\n");
        return false;
    }

    uint32_t recordCount;
    fread((void *)&recordCount, 4, 1, inFile);

    habit* data = new habit[recordCount];
    for (uint32_t i = 0; i < recordCount; i++) {
        if (sizeof(habit) != fread((void *)&data[i], 1, sizeof(habit), inFile)) {
            printf("Error reading extracted/sub_data/start/HABIT.dat!\n");
            return false;
        }
    }

    const char* size = "68\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t2\t4\t2\t15x2";
    const char* header = "name\tchanceRecovery\tchanceFist\tchanceSword\tchanceSpear\tchanceBow\tchanceGun\tchanceAxe\tchanceStaff\tchanceMeleeClaw\tchanceCasterClaw\t"
        "chanceArmor\tchanceBelt\tchanceShoes\tchanceOrb\tchanceGlasses\tchanceMuscle\tchanceWeight\tchanceMap\tchanceEmblem\tchanceLegendary\tunk21\tunk22\tid\tmaxStack\tid2\tempty";

    std::ofstream outFile("../extracted/txt/HABIT.txt");
    if (!outFile.is_open()) {
        printf("Error opening ../extracted/txt/HABIT.txt for output!\n");
        return false;
    }

    outFile << size << "\n" << header << "\n";
    for (uint32_t i = 0; i < recordCount; i++) {
        outFile << data[i].name << "\t" << (uint32_t)data[i].chanceRecovery << "\t" << (uint32_t)data[i].chanceFist << "\t" << (uint32_t)data[i].chanceSword << "\t" << (uint32_t)data[i].chanceSpear
            << "\t" << (uint32_t)data[i].chanceBow << "\t" << (uint32_t)data[i].chanceGun << "\t" << (uint32_t)data[i].chanceAxe << "\t" << (uint32_t)data[i].chanceStaff << "\t" << (uint32_t)data[i].chanceMeleeClaw
            << "\t" << (uint32_t)data[i].chanceCasterClaw << "\t" << (uint32_t)data[i].chanceArmor << "\t" << (uint32_t)data[i].chanceBelt << "\t" << (uint32_t)data[i].chanceShoes << "\t" << (uint32_t)data[i].chanceOrb
            << "\t" << (uint32_t)data[i].chanceGlasses << "\t" << (uint32_t)data[i].chanceMuscle << "\t" << (uint32_t)data[i].chanceWeight << "\t" << (uint32_t)data[i].chanceMap << "\t" << (uint32_t)data[i].chanceEmblem
            << "\t" << (uint32_t)data[i].chanceLegendary << "\t" << (uint32_t)data[i].unk21 << "\t" << (uint32_t)data[i].unk22 << "\t" << data[i].id << "\t" << data[i].maxStack << "\t" << data[i].id2 << "\t";
        for (uint32_t j = 0; j < 15; j++)
            outFile << data[i].empty[j] << " ";
        outFile << "\n";
    }
    outFile.close();
    return true;
}

bool MakeTxtMagic() {
    FILE *inFile;

    if (0 < fopen_s(&inFile, "../extracted/sub_data/start/magic.dat", "rb") || inFile == nullptr) {
        printf("Error opening ../extracted/sub_data/start/magic.dat for input!\n");
        return false;
    }

    uint32_t recordCount;
    fread((void *)&recordCount, 4, 1, inFile);

    magic* data = new magic[recordCount];
    for (uint32_t i = 0; i < recordCount; i++) {
        if (sizeof(magic) != fread((void *)&data[i], 1, sizeof(magic), inFile)) {
            printf("Error reading ../extracted/sub_data/start/magic.dat!\n");
            return false;
        }
    }

    const char* size = "2\t2\t2\t2\t2\t52\t148\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t2\t11x2";
    const char* header = "id\tunk1\tweaponLvl\tunk2\tcost\tname\tdesc\teffectValue1\teffectValue2\teffectValue3\teffectValue4\teffectValue5\teffectValue6\t"
        "weaponType\telement\tspellType\tweaponType2\tunk3\ttargetRange\ttargetInLine\ttargetShape\tminHeight\tmaxHeight\t"
        "effectType1\teffectType2\teffectType3\teffectType4\teffectType5\teffectType6\tid2\tempty";

    std::ofstream outFile("../extracted/txt/magic.txt");
    if (!outFile.is_open()) {
        printf("Error opening ../extracted/txt/magic.txt for output!\n");
        return false;
    }

    outFile << size << "\n" << header << "\n";
    for (uint32_t i = 0; i < recordCount; i++) {
        outFile << data[i].id << "\t" << data[i].unk1 << "\t" << data[i].weaponLvl << "\t" << data[i].unk2 << "\t" << data[i].cost << "\t" << data[i].name << "\t" << data[i].desc << "\t";
        for (uint32_t j = 0; j < 6; j++)
            outFile << (int32_t)data[i].effectValue[j] << "\t";
        outFile << (uint32_t)data[i].weaponType << "\t" << (uint32_t)data[i].element << "\t" << (uint32_t)data[i].spellType << "\t" << (uint32_t)data[i].weaponType2 << "\t" << (uint32_t)data[i].unk3
            << "\t" << (uint32_t)data[i].targetRange << "\t" << (uint32_t)data[i].targetInLine << "\t" << (uint32_t)data[i].targetShape << "\t" << (uint32_t)data[i].minHeight << "\t" << (uint32_t)data[i].maxHeight << "\t";
        for (uint32_t j = 0; j < 6; j++)
            outFile << (uint32_t)data[i].effectType[j] << "\t";
        outFile << data[i].id2 << "\t";
        for (uint32_t j = 0; j < 11; j++)
            outFile << data[i].empty[j] << " ";
        outFile << "\n";
    }
    outFile.close();
    return true;
}

bool MakeTxtMitem() {
    FILE *inFile;

    if (0 < fopen_s(&inFile, "../extracted/sub_data/start/mitem.dat", "rb") || inFile == nullptr) {
        printf("Error opening ../extracted/sub_data/start/mitem.dat for input!\n");
        return false;
    }

    uint32_t recordCount;
    fread((void *)&recordCount, 4, 1, inFile);

    mitem* data = new mitem[recordCount];
    for (uint32_t i = 0; i < recordCount; i++) {
        if (sizeof(mitem) != fread((void *)&data[i], 1, sizeof(mitem), inFile)) {
            printf("Error reading ../extracted/sub_data/start/mitem.dat!\n");
            return false;
        }
    }

    const char* size = "68\t136\t1\t1\t1\t1\t1\t1\t1\t1\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t4\t2\t3x2";
    const char* header = "name\tdesc\tstatid\tstatvalue\trank\trange\tjump\tunk4\tcritChance\tscrollSkill\ttype\tstatHP\tstatSP\tstatATK\tstatDEF\tstatINT\tstatSPD\tstatHIT\tstatRES\tid\ticon\ttype2\tmove\tspecialist\tsprite\tunk\tprice\tid2\tempty";

    std::ofstream outFile("../extracted/txt/mitem.txt");
    if (!outFile.is_open()) {
        printf("Error opening ../extracted/txt/mitem.txt for output!\n");
        return false;
    }

    outFile << size << "\n" << header << "\n";
    for (uint32_t i = 0; i < recordCount; i++)
        outFile << data[i].name << "\t" << data[i].desc << "\t" << (uint32_t)data[i].statid << "\t" << (int32_t)data[i].statvalue << "\t" << (uint32_t)data[i].rank
        << "\t" << (uint32_t)data[i].range << "\t" << (uint32_t)data[i].jump << "\t" << (uint32_t)data[i].unk4 << "\t" << (uint32_t)data[i].critChance << "\t" << (uint32_t)data[i].scrollSkill
        << "\t" << data[i].type << "\t" << data[i].statHP << "\t" << data[i].statSP << "\t" << data[i].statATK << "\t" << data[i].statDEF << "\t" << data[i].statINT << "\t" << data[i].statSPD
        << "\t" << data[i].statHIT << "\t" << data[i].statRES << "\t" << data[i].id << "\t" << data[i].icon << "\t" << data[i].type2 << "\t" << data[i].move << "\t" << data[i].specialist
        << "\t" << data[i].sprite << "\t" << data[i].unk << "\t" << data[i].price << "\t" << data[i].id2 << "\t" << data[i].empty[0] << " " << data[i].empty[1] << " " << data[i].empty[2] << " \n";
    outFile.close();
    return true;
}

bool MakeTxtDarkSun1() {
    FILE *inFile;

    if (0 < fopen_s(&inFile, "../extracted/sub_data/start_vm/darksun1.dat", "rb") || inFile == nullptr) {
        printf("Error opening ../extracted/sub_data/start_vm/darksun1.dat for input!\n");
        return false;
    }

    uint32_t recordCount;
    fread((void *)&recordCount, 4, 1, inFile);

    darksun1* data = new darksun1[recordCount];
    for (uint32_t i = 0; i < recordCount; i++) {
        if (sizeof(darksun1) != fread((void *)&data[i], 1, sizeof(darksun1), inFile)) {
            printf("Error reading ../extracted/sub_data/start_vm/darksun1.dat!\n");
            return false;
        }
    }

    const char* size = "44\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t2\t2\t2\t2\t17x2";
    const char* header = "name\tunk1\tunk2\tgroup1\tgroup2\tgroup3\tgroup4\tgroup5\tgroup6\tgroup7\tgroup8\tid\tunk3\ttarget\tid2\tempy";

    std::ofstream outFile("../extracted/txt/darksun1.txt");
    if (!outFile.is_open()) {
        printf("Error opening ../extracted/txt/darksun1.txt for output!\n");
        return false;
    }

    outFile << size << "\n" << header << "\n";
    for (uint32_t i = 0; i < recordCount; i++) {
        outFile << data[i].name << "\t" << (uint32_t)data[i].unk1 << "\t" << (uint32_t)data[i].unk2 << "\t" << (uint32_t)data[i].group[0] << "\t" << (uint32_t)data[i].group[1]
            << "\t" << (uint32_t)data[i].group[2] << "\t" << (uint32_t)data[i].group[3] << "\t" << (uint32_t)data[i].group[4] << "\t" << (uint32_t)data[i].group[5]
            << "\t" << (uint32_t)data[i].group[6] << "\t" << (uint32_t)data[i].group[7] << "\t" << data[i].id << "\t" << data[i].unk3 << "\t" << data[i].target << "\t" << data[i].id2 << "\t";
        for (uint32_t j = 0; j < 17; j++)
            outFile << data[i].empty[j] << " ";
        outFile << "\n";
    }
    outFile.close();
    return true;
}

bool MakeTxtDarkSun2() {
    FILE *inFile;

    if (0 < fopen_s(&inFile, "../extracted/sub_data/start_vm/darksun2.dat", "rb") || inFile == nullptr) {
        printf("Error opening ../extracted/sub_data/start_vm/darksun2.dat for input!\n");
        return false;
    }

    uint32_t recordCount;
    fread((void *)&recordCount, 4, 1, inFile);

    darksun2* data = new darksun2[recordCount];
    for (uint32_t i = 0; i < recordCount; i++) {
        if (sizeof(darksun2) != fread((void *)&data[i], 1, sizeof(darksun2), inFile)) {
            printf("Error reading ../extracted/sub_data/start_vm/darksun2.dat!\n");
            return false;
        }
    }

    const char* size = "52\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t2\t2\t2\t2\t13x2";
    const char* header = "name\tunk1\tgroup1\tgroup2\tgroup3\tgroup4\tgroup5\tgroup6\tgroup7\tgroup8\tunk2\tid\tunk3\ttarget\tid2\tempy";

    std::ofstream outFile("../extracted/txt/darksun2.txt");
    if (!outFile.is_open()) {
        printf("Error opening ../extracted/txt/darksun2.txt for output!\n");
        return false;
    }

    outFile << size << "\n" << header << "\n";
    for (uint32_t i = 0; i < recordCount; i++) {
        outFile << data[i].name << "\t" << (uint32_t)data[i].unk1 << "\t" << (uint32_t)data[i].group[0] << "\t" << (uint32_t)data[i].group[1] << "\t" << (uint32_t)data[i].group[2]
            << "\t" << (uint32_t)data[i].group[3] << "\t" << (uint32_t)data[i].group[4] << "\t" << (uint32_t)data[i].group[5] << "\t" << (uint32_t)data[i].group[6]
            << "\t" << (uint32_t)data[i].group[7] << "\t" << (uint32_t)data[i].unk2 << "\t" << data[i].id << "\t" << data[i].unk3 << "\t" << data[i].target << "\t" << data[i].id2 << "\t";
        for (uint32_t j = 0; j < 13; j++)
            outFile << data[i].empty[j] << " ";
        outFile << "\n";
    }
    outFile.close();
    return true;
}

bool MakeTxtDkTable() {
    FILE *inFile;

    if (0 < fopen_s(&inFile, "../extracted/sub_data/start_vm/dktable.dat", "rb") || inFile == nullptr) {
        printf("Error opening ../extracted/sub_data/start_vm/dktable.dat for input!\n");
        return false;
    }

    uint32_t recordCount;
    fread((void *)&recordCount, 4, 1, inFile);
    fread((void *)&recordCount, 4, 1, inFile); //count is written twice for some reason

    dktable* data = new dktable[recordCount];
    for (uint32_t i = 0; i < recordCount; i++) {
        if (sizeof(dktable) != fread((void *)&data[i], 1, sizeof(dktable), inFile)) {
            printf("Error reading ../extracted/sub_data/start_vm/dktable.dat!\n");
            return false;
        }
    }

    const char* size = "2\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1";
    const char* header = "mapId\tcount\tdifficulty\t"
        "group1\tgroup2\tgroup3\tgroup4\tgroup5\tgroup6\tgroup7\tgroup8\tgroup9\tgroup10\tgroup11\tgroup12\tgroup13\tgroup14\tgroup15\tgroup16\t"
        "group17\tgroup18\tgroup19\tgroup20\tgroup21\tgroup22\tgroup23\tgroup24\tgroup25\tgroup26\tgroup27\tgroup28\tgroup29\tgroup30\tgroup31\tgroup32\t"
        "sun1\tsun2\tsun3\tsun4\tsun5\tsun6\tsun7\tsun8\tsun9\tsun10\tsun11\tsun12\tsun13\tsun14\tsun15\tsun16\t"
        "sun17\tsun18\tsun19\tsun20\tsun21\tsun22\tsun23\tsun24\tsun25\tsun26\tsun27\tsun28\tsun29\tsun30\tsun31\tsun32\t"
        "effect1\teffect2\teffect3\teffect4\teffect5\teffect6\teffect7\teffect8\teffect9\teffect10\teffect11\teffect12\t"
        "effect13\teffect14\teffect15\teffect16\teffect17\teffect18\teffect19\teffect20\teffect21\teffect22\t"
        "effect23\teffect24\teffect25\teffect26\teffect27\teffect28\teffect29\teffect30\teffect31\teffect32\t"
        "empty1\tempty2\tempty3\tempty4\tempty5\tempty6\tempty7\tempty8\tempty9\tempty10\tempty11\tempty12\tempty13\tempty14\tempty15\tempty16\t"
        "empty17\tempty18\tempty19\tempty20\tempty21\tempty22\tempty23\tempty24\tempty25\tempty26\tempty27\tempty28\tempty29\tempty30\tempty31\tempty32";

    std::ofstream outFile("../extracted/txt/dktable.txt");
    if (!outFile.is_open()) {
        printf("Error opening ../extracted/txt/dktable.txt for output!\n");
        return false;
    }

    outFile << size << "\n" << header << "\n";
    for (uint32_t i = 0; i < recordCount; i++) {
        outFile << data[i].mapId << "\t" << (uint32_t)data[i].count << "\t" << (uint32_t)data[i].difficulty;
        for (uint32_t j = 0; j < 32; j++)
            outFile << "\t" << (uint32_t)data[i].dkGroup[j];
        for (uint32_t j = 0; j < 32; j++)
            outFile << "\t" << data[i].dkSun[j];
        for (uint32_t j = 0; j < 32; j++)
            outFile << "\t" << (uint32_t)data[i].dkEffect[j];
        for (uint32_t j = 0; j < 32; j++)
            outFile << "\t" << (uint32_t)data[i].dkEmpty[j];
        outFile << "\n";
    }
    outFile.close();
    return true;
}

bool MakeTxtHospital() {
    FILE *inFile;

    if (0 < fopen_s(&inFile, "../extracted/sub_data/start_vm/HOSPITAL.dat", "rb") || inFile == nullptr) {
        printf("Error opening ../extracted/sub_data/start_vm/HOSPITAL.dat for input!\n");
        return false;
    }

    uint32_t recordCount;
    fread((void *)&recordCount, 4, 1, inFile);
    fread((void *)&recordCount, 4, 1, inFile); //count is written twice for some reason

    hospital* data = new hospital[recordCount];
    for (uint32_t i = 0; i < recordCount; i++) {
        if (sizeof(hospital) != fread((void *)&data[i], 1, sizeof(hospital), inFile)) {
            printf("Error reading ../extracted/sub_data/start_vm/HOSPITAL.dat!\n");
            return false;
        }
    }

    const char* size = "2\t2\t4\t4\t4";
    const char* header = "id\trevive\treward\trestoreHP\trestoreSP";

    std::ofstream outFile("../extracted/txt/HOSPITAL.txt");
    if (!outFile.is_open()) {
        printf("Error opening ../extracted/txt/HOSPITAL.txt for output!\n");
        return false;
    }

    outFile << size << "\n" << header << "\n";
    for (uint32_t i = 0; i < recordCount; i++)
        outFile << data[i].id << "\t" << data[i].revive << "\t" << data[i].reward << "\t" << data[i].restoreHP << "\t" << data[i].restoreSP << "\n";
    outFile.close();
    return true;
}

bool MakeTxtJudgment() {
    FILE *inFile;

    if (0 < fopen_s(&inFile, "../extracted/sub_data/start_vm/judgment.dat", "rb") || inFile == nullptr) {
        printf("Error opening ../extracted/sub_data/start_vm/judgment.dat for input!\n");
        return false;
    }

    uint32_t recordCount;
    fread((void *)&recordCount, 4, 1, inFile);

    judgment* data = new judgment[recordCount];
    for (uint32_t i = 0; i < recordCount; i++) {
        if (sizeof(judgment) != fread((void *)&data[i], 1, sizeof(judgment), inFile)) {
            printf("Error reading extracted/sub_data/start_vm/judgment.dat!\n");
            return false;
        }
    }

    const char* size = "68\t1\t1\t1\t1\t1\t19x1\t4\t4\t1\t27x1";
    const char* header = "name\tid\tworldLevel\tfelonyCount\tgoalType\trewardType\tempty\tgoal\treward\tid2\tempty";

    std::ofstream outFile("../extracted/txt/judgment.txt");
    if (!outFile.is_open()) {
        printf("Error opening ../extracted/txt/judgment.txt for output!\n");
        return false;
    }

    outFile << size << "\n" << header << "\n";
    for (uint32_t i = 0; i < recordCount; i++) {
        outFile << data[i].name << "\t" << (uint32_t)data[i].id << "\t" << (uint32_t)data[i].worldLevel << "\t"
            << (uint32_t)data[i].felonyCount << "\t" << (uint32_t)data[i].goalType << "\t" << (uint32_t)data[i].rewardType << "\t";
        for (uint32_t j = 0; j < 19; j++)
            outFile << (uint32_t)data[i].empty[j] << " ";
        outFile << "\t" << data[i].goal << "\t" << data[i].reward << "\t" << (uint32_t)data[i].id2 << "\t";
        for (uint32_t j = 0; j < 27; j++)
            outFile << (uint32_t)data[i].empty2[j] << " ";
        outFile << "\n";
    }
    outFile.close();
    return true;
}

bool MakeTxtPirate() {
    FILE *inFile;

    if (0 < fopen_s(&inFile, "../extracted/sub_data/start_vm/pirate.dat", "rb") || inFile == nullptr) {
        printf("Error opening ../extracted/sub_data/start_vm/pirate.dat for input!\n");
        return false;
    }

    uint32_t recordCount;
    fread((void *)&recordCount, 4, 1, inFile);

    pirate* data = new pirate[recordCount];
    for (uint32_t i = 0; i < recordCount; i++) {
        if (sizeof(pirate) != fread((void *)&data[i], 1, sizeof(pirate), inFile)) {
            printf("Error reading extracted/sub_data/start_vm/pirate.dat!\n");
            return false;
        }
    }

    const char* size = "44\t2\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t2\t7x2";
    const char* header = "name\tid\tworldLevel\tspecialists\ttreasures\tunk1\tcrewSize\tshipType\tchanceRecovery\tchanceFist\tchanceSword\tchanceSpear\tchanceBow\tchanceGun\tchanceAxe\tchanceStaff\t"
        "chanceMeleeClaw\tchanceCasterClaw\tchanceArmor\tchanceBelt\tchanceShoes\tchanceOrb\tchanceGlasses\tchanceMuscle\tchanceWeight\tchanceMap\tchanceEmblem\tchanceLegendary\tunk2\tunk3\tunk4\tunk5\t"
        "pirateId\tpirateLvlMult\tcrewId1\tcrewId2\tcrewId3\tcrewId4\tcrewId5\tcrewId6\tcrewId7\tcrewId8\tcrewId9\tcrewId10\tcrewId11\tcrewId12\tcrewId13\tcrewId14\tcrewId15\tcrewId16\tcrewLvlMult1\tcrewLvlMult2\t"
        "crewLvlMult3\tcrewLvlMult4\tcrewLvlMult5\tcrewLvlMult6\tcrewLvlMult7\tcrewLvlMult8\tcrewLvlMult9\tcrewLvlMult10\tcrewLvlMult11\tcrewLvlMult12\tcrewLvlMult13\tcrewLvlMult14\tcrewLvlMult15\tcrewLvlMult16\tid2\tempty";

    std::ofstream outFile("../extracted/txt/pirate.txt");
    if (!outFile.is_open()) {
        printf("Error opening ../extracted/txt/pirate.txt for output!\n");
        return false;
    }

    outFile << size << "\n" << header << "\n";
    for (uint32_t i = 0; i < recordCount; i++) {
        outFile << data[i].name << "\t" << data[i].id << "\t" << (uint32_t)data[i].worldLevel << "\t" << (uint32_t)data[i].specialists << "\t" << (uint32_t)data[i].treasures
            << "\t" << (uint32_t)data[i].unk1 << "\t" << (uint32_t)data[i].crewSize << "\t" << (uint32_t)data[i].shipType
            << "\t" << (uint32_t)data[i].chanceRecovery << "\t" << (uint32_t)data[i].chanceFist << "\t" << (uint32_t)data[i].chanceSword << "\t" << (uint32_t)data[i].chanceSpear
            << "\t" << (uint32_t)data[i].chanceBow << "\t" << (uint32_t)data[i].chanceGun << "\t" << (uint32_t)data[i].chanceAxe << "\t" << (uint32_t)data[i].chanceStaff << "\t" << (uint32_t)data[i].chanceMeleeClaw
            << "\t" << (uint32_t)data[i].chanceCasterClaw << "\t" << (uint32_t)data[i].chanceArmor << "\t" << (uint32_t)data[i].chanceBelt << "\t" << (uint32_t)data[i].chanceShoes << "\t" << (uint32_t)data[i].chanceOrb
            << "\t" << (uint32_t)data[i].chanceGlasses << "\t" << (uint32_t)data[i].chanceMuscle << "\t" << (uint32_t)data[i].chanceWeight << "\t" << (uint32_t)data[i].chanceMap << "\t" << (uint32_t)data[i].chanceEmblem
            << "\t" << (uint32_t)data[i].chanceLegendary << "\t" << (uint32_t)data[i].unk2 << "\t" << (uint32_t)data[i].unk3 << "\t" << (uint32_t)data[i].unk4 << "\t" << (uint32_t)data[i].unk5
            << "\t" << data[i].pirateId << "\t" << data[i].pirateLvlMult << "\t";
        for (uint32_t j = 0; j < 16; j++)
            outFile << data[i].crewId[j] << "\t";
        for (uint32_t j = 0; j < 16; j++)
            outFile << data[i].crewLvlMult[j] << "\t";
        outFile << data[i].id2 << "\t";
        for (uint32_t j = 0; j < 7; j++)
            outFile << data[i].empty[j] << " ";
        outFile << "\n";
    }
    outFile.close();
    return true;
}

bool MakeTxtWish() {
    FILE *inFile;

    if (0 < fopen_s(&inFile, "../extracted/sub_data/start_vm/WISH.dat", "rb") || inFile == nullptr) {
        printf("Error opening ../extracted/sub_data/start_vm/WISH.dat for input!\n");
        return false;
    }

    uint32_t recordCount;
    fread((void *)&recordCount, 4, 1, inFile);

    wish* data = new wish[recordCount];
    for (uint32_t i = 0; i < recordCount; i++) {
        if (sizeof(wish) != fread((void *)&data[i], 1, sizeof(wish), inFile)) {
            printf("Error reading ../extracted/sub_data/start_vm/WISH.dat!\n");
            return false;
        }
    }

    const char* size = "2\t2\t2\t2\t1\t1\t68\t126\t2\t25x2";
    const char* header = "cost\tunk1\tid\tsenators\tlegendary\tapproval\tname\tdesc\tid2\tempty";

    std::ofstream outFile("../extracted/txt/WISH.txt");
    if (!outFile.is_open()) {
        printf("Error opening ../extracted/txt/WISH.txt for output!\n");
        return false;
    }

    outFile << size << "\n" << header << "\n";
    for (uint32_t i = 0; i < recordCount; i++) {
        outFile << data[i].cost << "\t" << data[i].unk1 << "\t" << data[i].id << "\t" << data[i].senators << "\t" << (uint32_t)data[i].legendary
            << "\t" << (int32_t)data[i].approval << "\t" << data[i].name << "\t" << data[i].desc << "\t" << data[i].id2 << "\t";
        for (uint32_t j = 0; j < 25; j++)
            outFile << (uint32_t)data[i].empty[j] << " ";
        outFile << "\n";
    }
    outFile.close();
    return true;
}

bool MakeTxtWish2() {
    FILE *inFile;

    if (0 < fopen_s(&inFile, "../extracted/sub_data/start/WISH2.dat", "rb") || inFile == nullptr) {
        printf("Error opening ../extracted/sub_data/start/WISH2.dat for input!\n");
        return false;
    }

    uint32_t recordCount;
    fread((void *)&recordCount, 4, 1, inFile);

    wish2* data = new wish2[recordCount];
    for (uint32_t i = 0; i < recordCount; i++) {
        if (sizeof(wish2) != fread((void *)&data[i], 1, sizeof(wish2), inFile)) {
            printf("Error reading ../extracted/sub_data/start/WISH2.dat!\n");
            return false;
        }
    }

    const char* size = "4\t4\t4\t2\t8x2\t2\t1\t1\t1\t2x1\t1\t104\t126\t1\t51x1";
    const char* header = "unk1\tunk2\tunk3\tproposerId\tempty\tunk4\tid\tgroup\tunk5\tempty\tunk6\tname\tdesc\tid2\tempty";

    std::ofstream outFile("../extracted/txt/WISH2.txt");
    if (!outFile.is_open()) {
        printf("Error opening ../extracted/txt/WISH2.txt for output!\n");
        return false;
    }

    outFile << size << "\n" << header << "\n";
    for (uint32_t i = 0; i < recordCount; i++) {
        outFile << data[i].unk1 << "\t" << data[i].unk2 << "\t" << data[i].unk3 << "\t" << data[i].proposerId << "\t";
        for (uint32_t j = 0; j < 8; j++)
            outFile << data[i].empty[j] << " ";
        outFile << "\t" << data[i].unk4 << "\t" << (uint32_t)data[i].id << "\t" << (uint32_t)data[i].group << "\t" << (uint32_t)data[i].unk5
            << "\t" << (uint32_t)data[i].empty2[0] << " " << (uint32_t)data[i].empty2[1] << " \t" << (uint32_t)data[i].unk6
            << "\t" << data[i].name << "\t" << data[i].desc << "\t" << (uint32_t)data[i].id2 << "\t";
        for (uint32_t j = 0; j < 51; j++)
            outFile << (uint32_t)data[i].empty3[j] << " ";
        outFile << "\n";
    }
    outFile.close();
    return true;
}

bool MakeStartTxtFiles() {
    if (!MakeTxtChar())
        return false;
    if (!MakeTxtCharHelp())
        return false;
    if (!MakeTxtDungeon())
        return false;
    if (!MakeTxtHabit())
        return false;
    if (!MakeTxtMagic())
        return false;
    if (!MakeTxtMitem())
        return false;
    if (!MakeTxtDarkSun1())
        return false;
    if (!MakeTxtDarkSun2())
        return false;
    if (!MakeTxtDkTable())
        return false;
    if (!MakeTxtHospital())
        return false;
    if (!MakeTxtJudgment())
        return false;
    if (!MakeTxtPirate())
        return false;
    if (!MakeTxtWish())
        return false;
    if (!MakeTxtWish2())
        return false;
    return true;
}