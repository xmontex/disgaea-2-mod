#include <windows.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include "lzs.h"
#include "Animation.h"
#include "../shared/DataStructure.h"
#include "../shared/AnimationStructure.h"

const std::filesystem::path dataPath("../extracted/sub_data/anmpack/large_raw");
const std::filesystem::path infoPath("../extracted/sub_data/anmpack/info.txt");

bool ExtractAnimationData(std::filesystem::path inFileName) {
    FILE* dataFile, * outFile;
    if (0 < fopen_s(&dataFile, inFileName.string().c_str(), "rb") || dataFile == nullptr) {
        printf("Error opening %s for input!\n", inFileName.string().c_str());
        return false;
    }
    Count count;
    fread((void*)&count, sizeof(Count), 1, dataFile);

    std::ofstream infoFile(infoPath, std::ios::out | std::ios::app);
    if (!infoFile.is_open()) {
        printf("Error opening ../extracted/txt/char.txt for output!\n");
        return false;
    }

    for (uint32_t cnt = 0; cnt < count.count; cnt++) {
        const uint32_t animationOffset = ftell(dataFile);
        Animation animation;
        fread((void*)&animation.header, sizeof(Header), 1, dataFile);
        animation.firstChunk = new FirstChunk[animation.header.firstChunkCount];
        animation.actionChunk = new ActionChunk[animation.header.secondChunkCount];
        animation.actionFrameChunk = new ActionFrameChunk[animation.header.thirdChunkCount];
        animation.paletteChunk = new FourthChunk[animation.header.fourthChunkCount];
        animation.imageChunk = new FifthChunk[animation.header.fifthChunkCount];
        animation.animationChunk = new SixthChunk[animation.header.sixthChunkCount];
        animation.frameChunk = new SeventhChunk[animation.header.seventhChunkCount];
        animation.eighthChunk = new EighthChunk[animation.header.eighthChunkCount];
        animation.ninthChunk = new NinthChunk[animation.header.ninthChunkCount];
        animation.palettes = new Palette[animation.header.fourthChunkCount];
        animation.images = new Image[animation.header.fifthChunkCount];

        fseek(dataFile, animation.header.firstChunkStart + animationOffset, SEEK_SET);
        fread((void*)animation.firstChunk, sizeof(FirstChunk), animation.header.firstChunkCount, dataFile);
        fseek(dataFile, animation.header.secondChunkStart + animationOffset, SEEK_SET);
        fread((void*)animation.actionChunk, sizeof(ActionChunk), animation.header.secondChunkCount, dataFile);
        fseek(dataFile, animation.header.thirdChunkStart + animationOffset, SEEK_SET);
        fread((void*)animation.actionFrameChunk, sizeof(ActionChunk), animation.header.thirdChunkCount, dataFile);
        fseek(dataFile, animation.header.fourthChunkStart + animationOffset, SEEK_SET);
        fread((void*)animation.paletteChunk, sizeof(FourthChunk), animation.header.fourthChunkCount, dataFile);
        fseek(dataFile, animation.header.fifthChunkStart + animationOffset, SEEK_SET);
        fread((void*)animation.imageChunk, sizeof(FifthChunk), animation.header.fifthChunkCount, dataFile);
        fseek(dataFile, animation.header.sixthChunkStart + animationOffset, SEEK_SET);
        fread((void*)animation.animationChunk, sizeof(SixthChunk), animation.header.sixthChunkCount, dataFile);
        fseek(dataFile, animation.header.seventhChunkStart + animationOffset, SEEK_SET);
        fread((void*)animation.frameChunk, sizeof(SeventhChunk), animation.header.seventhChunkCount, dataFile);
        fseek(dataFile, animation.header.eighthChunkStart + animationOffset, SEEK_SET);
        fread((void*)animation.eighthChunk, sizeof(EighthChunk), animation.header.eighthChunkCount, dataFile);
        fseek(dataFile, animation.header.ninthChunkStart + animationOffset, SEEK_SET);
        fread((void*)animation.ninthChunk, sizeof(NinthChunk), animation.header.ninthChunkCount, dataFile);

       
        for (uint32_t i = 0; i < animation.header.firstChunkCount; i++)
            infoFile << animation.header.index << "\t" << 1 << "\t" << i << "\t" << animation.firstChunk[i].offset << "\t" << animation.firstChunk[i].start << "\t" << animation.firstChunk[i].end << "\t" << animation.firstChunk[i].empty << "\n";
        
        for (uint32_t i = 0; i < animation.header.secondChunkCount; i++)
            infoFile << animation.header.index << "\t" << 2 << "\t" << i << "\t" << (uint32_t)animation.actionChunk[i].unk1 << "\t" << (uint32_t)animation.actionChunk[i].unk2 << "\t" << (uint32_t)animation.actionChunk[i].unk3 << "\t"
                << (uint32_t)animation.actionChunk[i].unk4 << "\t" << animation.actionChunk[i].thirdChunkId << "\t" << animation.actionChunk[i].thirdChunkCount << "\t" << animation.actionChunk[i].unk7 << "\t" << animation.actionChunk[i].unk8 << "\n";

        for (uint32_t i = 0; i < animation.header.thirdChunkCount; i++)
            infoFile << animation.header.index << "\t" << 3 << "\t" << i << "\t" << animation.actionFrameChunk[i].unk1 << "\t" << animation.actionFrameChunk[i].unk2 << "\t"
                << animation.actionFrameChunk[i].sixthId << "\t" << animation.actionFrameChunk[i].seventhId << "\t" << animation.actionFrameChunk[i].unk5 << "\t" << animation.actionFrameChunk[i].unk6  << "\n";

        for (uint32_t i = 0; i < animation.header.fourthChunkCount; i++) {
            infoFile << animation.header.index << "\t" << 4 << "\t" << i << "\t" << animation.paletteChunk[i].offset << "\t" << animation.paletteChunk[i].colorCount << "\n";

            animation.palettes[i].colors = new Color[256];
            fseek(dataFile, animation.paletteChunk[i].offset + animationOffset, SEEK_SET);
            fread((void*)animation.palettes[i].colors, sizeof(Color), animation.paletteChunk[i].colorCount, dataFile);
            std::stringstream end;
            end << "small_raw/anm" << std::setw(4) << std::setfill('0') << animation.header.index << ".pal" << i;
            std::filesystem::path outFileName(inFileName);
            outFileName.replace_filename(end.str());
            if (0 < fopen_s(&outFile, outFileName.string().c_str(), "wb") || outFile == nullptr) {
                printf("Error opening %s for output!\n", outFileName.string().c_str());
                return false;
            }
            fwrite(animation.palettes[i].colors, sizeof(Color), animation.paletteChunk[i].colorCount, outFile);
            fclose(outFile);
            printf("Write %s: %i!\n", outFileName.string().c_str(), animation.paletteChunk[i].colorCount);

            for (uint32_t j = animation.paletteChunk[i].colorCount; j < 256; j++) {
                animation.palettes[i].colors[j].r = 255;
                animation.palettes[i].colors[j].g = 255;
                animation.palettes[i].colors[j].b = 255;
                animation.palettes[i].colors[j].a = 255;
            }
            animation.paletteChunk[i].colorCount = 256;
        }

        for (uint32_t i = 0; i < animation.header.fifthChunkCount; i++) {
            infoFile << animation.header.index << "\t" << 5 << "\t" << i << "\t" << animation.imageChunk[i].offset << "\t" << animation.imageChunk[i].width << "\t" << animation.imageChunk[i].height << "\t"
                << (uint32_t)animation.imageChunk[i].bitsPerPixel << "\t" << (uint32_t)animation.imageChunk[i].unk1 << "\t" << (uint32_t)animation.imageChunk[i].unk2 << "\t" << (uint32_t)animation.imageChunk[i].empty << "\n";

            const uint32_t dataSize = animation.imageChunk[i].width * animation.imageChunk[i].height;
            animation.images[i].data = new uint8_t[dataSize];
            fseek(dataFile, animation.imageChunk[i].offset + animationOffset, SEEK_SET);
            if (animation.imageChunk[i].bitsPerPixel == 4) {
                fread((void*)animation.images[i].data, sizeof(uint8_t), dataSize * animation.imageChunk[i].bitsPerPixel / 8, dataFile);
                for (int32_t j = dataSize / 2 - 1; j >= 0; j--) {
                    uint8_t part1 = animation.images[i].data[j] >> 4;
                    uint8_t part2 = animation.images[i].data[j] & 0xF;
                    animation.images[i].data[j * 2] = part2;
                    animation.images[i].data[j * 2 + 1] = part1;
                }
            }
            else
                fread((void*)animation.images[i].data, sizeof(uint8_t), dataSize, dataFile);

            for (uint32_t j = 0; j < animation.header.fourthChunkCount; j++) {
                std::stringstream end;
                end << "small_raw/anm" << std::setw(4) << std::setfill('0') << animation.header.index << "_" << i << "_" << j << ".bmp";
                std::filesystem::path outFileName(inFileName);
                outFileName.replace_filename(end.str());
                if (0 < fopen_s(&outFile, outFileName.string().c_str(), "wb") || outFile == nullptr) {
                    printf("Error opening %s for output!\n", outFileName.string().c_str());
                    return false;
                }
                
                const uint16_t planes = 1;
                const uint16_t bitCount = 8;
                const uint32_t compression = BI_RGB;
                const uint32_t pixelsPerMeter = 2835;
                const uint32_t colorCount = 256;
                const uint32_t width = animation.imageChunk[i].width;
                const int32_t height = -animation.imageChunk[i].height;
                const uint32_t infoSize = sizeof(BITMAPINFOHEADER);
                const uint32_t headerSize = sizeof(BITMAPFILEHEADER) + infoSize + sizeof(RGBQUAD) * colorCount;
                const uint32_t totalSize = headerSize + dataSize;
                //BITMAPFILEHEADER
                fwrite("BM", 1, 2, outFile);
                fwrite((void*)&totalSize, 4, 1, outFile);
                fwrite("\0\0\0\0", 4, 1, outFile);
                fwrite((void*)&headerSize, 4, 1, outFile);
                //BITMAPINFOHEADER
                fwrite((void*)&infoSize, 4, 1, outFile);
                fwrite((void*)&width, 4, 1, outFile);
                fwrite((void*)&height, 4, 1, outFile);
                fwrite((void*)&planes, 2, 1, outFile);
                fwrite((void*)&bitCount, 2, 1, outFile);
                fwrite((void*)&compression, 4, 1, outFile);
                fwrite((void*)&dataSize, 4, 1, outFile);
                fwrite((void*)&pixelsPerMeter, 4, 1, outFile);
                fwrite((void*)&pixelsPerMeter, 4, 1, outFile);
                fwrite((void*)&colorCount, 4, 1, outFile);
                fwrite((void*)&colorCount, 4, 1, outFile);

                fwrite((void*)animation.palettes[j].colors, sizeof(RGBQUAD), colorCount, outFile);
                fwrite(animation.images[i].data, 1, dataSize, outFile);
                fclose(outFile);
                printf("Write %s!\n", outFileName.string().c_str());
            }
        }

        for (uint32_t i = 0; i < animation.header.sixthChunkCount; i++)
            infoFile << animation.header.index << "\t" << 6 << "\t" << i << "\t" << animation.animationChunk[i].unk1 << "\t" << animation.animationChunk[i].unk2 << "\t" << (int)animation.animationChunk[i].unk3 << "\t" << (int)animation.animationChunk[i].unk4 << "\t"
                << (int)animation.animationChunk[i].unk5 << "\t" << (int)animation.animationChunk[i].unk6 << "\t" << animation.animationChunk[i].left << "\t" << animation.animationChunk[i].top << "\t" << animation.animationChunk[i].width << "\t" << animation.animationChunk[i].height << "\n";

        for (uint32_t i = 0; i < animation.header.seventhChunkCount; i++)
            infoFile << animation.header.index << "\t" << 7 << "\t" << i << "\t" << animation.frameChunk[i].unk1 << "\t" << animation.frameChunk[i].unk2 << "\t" << animation.frameChunk[i].offsetX << "\t"
                << animation.frameChunk[i].offsetY << "\t" << animation.frameChunk[i].scaleX << "\t" << animation.frameChunk[i].scaleY << "\t" << animation.frameChunk[i].unk3 << "\n";

        for (uint32_t i = 0; i < animation.header.eighthChunkCount; i++)
            infoFile << animation.header.index << "\t" << 8 << "\t" << i << "\t" << animation.eighthChunk[i].unk1 << "\t" << animation.eighthChunk[i].unk2 << "\t" << animation.eighthChunk[i].unk3 << "\t" 
                << animation.eighthChunk[i].unk4 << "\t" << animation.eighthChunk[i].unk5 << "\t" << animation.eighthChunk[i].unk6 << "\t" << animation.eighthChunk[i].unk7 << "\t" << animation.eighthChunk[i].unk8 << "\n";

        for (uint32_t i = 0; i < animation.header.ninthChunkCount; i++)
            infoFile << animation.header.index << "\t" << 9 << "\t" << i << "\t" << animation.ninthChunk[i].unk1 << "\n";

        fseek(dataFile, animation.header.fileSize + animationOffset, SEEK_SET);
    }
    infoFile.close();
    fclose(dataFile);
    return true;
}

bool ConvertAnimationDataToHD(std::filesystem::path inFileName) {
    FILE* dataFile;
    if (0 < fopen_s(&dataFile, inFileName.string().c_str(), "rb") || dataFile == nullptr) {
        printf("Error opening %s for input!\n", inFileName.string().c_str());
        return false;
    }

    Count count;
    fread((void*)&count, sizeof(Count), 1, dataFile);

    if (count.count > 1)
        printf("File %s has %u animations!\n", inFileName.string().c_str(), count.count);

    Animation* animations = new Animation[count.count];
    for (uint32_t cnt = 0; cnt < count.count; cnt++) {
        const uint32_t animationOffset = ftell(dataFile);
        animations[cnt] = Animation();
        fread((void*)&animations[cnt].header, sizeof(Header), 1, dataFile);
        animations[cnt].firstChunk = new FirstChunk[animations[cnt].header.firstChunkCount];
        animations[cnt].actionChunk = new ActionChunk[animations[cnt].header.secondChunkCount];
        animations[cnt].actionFrameChunk = new ActionFrameChunk[animations[cnt].header.thirdChunkCount];
        animations[cnt].paletteChunk = new FourthChunk[animations[cnt].header.fourthChunkCount];
        animations[cnt].imageChunk = new FifthChunk[animations[cnt].header.fifthChunkCount];
        animations[cnt].animationChunk = new SixthChunk[animations[cnt].header.sixthChunkCount];
        animations[cnt].frameChunk = new SeventhChunk[animations[cnt].header.seventhChunkCount];
        animations[cnt].eighthChunk = new EighthChunk[animations[cnt].header.eighthChunkCount];
        animations[cnt].ninthChunk = new NinthChunk[animations[cnt].header.ninthChunkCount];
        animations[cnt].palettes = new Palette[animations[cnt].header.fourthChunkCount];
        animations[cnt].images = new Image[animations[cnt].header.fifthChunkCount];

        fseek(dataFile, animations[cnt].header.firstChunkStart + animationOffset, SEEK_SET);
        fread((void*)animations[cnt].firstChunk, sizeof(FirstChunk), animations[cnt].header.firstChunkCount, dataFile);
        fseek(dataFile, animations[cnt].header.secondChunkStart + animationOffset, SEEK_SET);
        fread((void*)animations[cnt].actionChunk, sizeof(ActionChunk), animations[cnt].header.secondChunkCount, dataFile);
        fseek(dataFile, animations[cnt].header.thirdChunkStart + animationOffset, SEEK_SET);
        fread((void*)animations[cnt].actionFrameChunk, sizeof(ActionFrameChunk), animations[cnt].header.thirdChunkCount, dataFile);
        fseek(dataFile, animations[cnt].header.fourthChunkStart + animationOffset, SEEK_SET);
        fread((void*)animations[cnt].paletteChunk, sizeof(FourthChunk), animations[cnt].header.fourthChunkCount, dataFile);
        fseek(dataFile, animations[cnt].header.fifthChunkStart + animationOffset, SEEK_SET);
        fread((void*)animations[cnt].imageChunk, sizeof(FifthChunk), animations[cnt].header.fifthChunkCount, dataFile);
        fseek(dataFile, animations[cnt].header.sixthChunkStart + animationOffset, SEEK_SET);
        fread((void*)animations[cnt].animationChunk, sizeof(SixthChunk), animations[cnt].header.sixthChunkCount, dataFile);
        fseek(dataFile, animations[cnt].header.seventhChunkStart + animationOffset, SEEK_SET);
        fread((void*)animations[cnt].frameChunk, sizeof(SeventhChunk), animations[cnt].header.seventhChunkCount, dataFile);
        fseek(dataFile, animations[cnt].header.eighthChunkStart + animationOffset, SEEK_SET);
        fread((void*)animations[cnt].eighthChunk, sizeof(EighthChunk), animations[cnt].header.eighthChunkCount, dataFile);
        fseek(dataFile, animations[cnt].header.ninthChunkStart + animationOffset, SEEK_SET);
        fread((void*)animations[cnt].ninthChunk, sizeof(NinthChunk), animations[cnt].header.ninthChunkCount, dataFile);

        for (uint32_t i = 0; i < animations[cnt].header.fourthChunkCount; i++) {
            animations[cnt].palettes[i].colors = new Color[256];
            fseek(dataFile, animations[cnt].paletteChunk[i].offset + animationOffset, SEEK_SET);
            fread((void*)animations[cnt].palettes[i].colors, sizeof(Color), animations[cnt].paletteChunk[i].colorCount, dataFile);
            for (uint32_t j = animations[cnt].paletteChunk[i].colorCount; j < 256; j++) {
                animations[cnt].palettes[i].colors[j].r = 255;
                animations[cnt].palettes[i].colors[j].g = 255;
                animations[cnt].palettes[i].colors[j].b = 255;
                animations[cnt].palettes[i].colors[j].a = 255;
            }
        }

        for (uint32_t i = 0; i < animations[cnt].header.fifthChunkCount; i++) {
            const uint32_t size = animations[cnt].imageChunk[i].width * animations[cnt].imageChunk[i].height;
            animations[cnt].images[i].data = new uint8_t[size];
            fseek(dataFile, animations[cnt].imageChunk[i].offset + animationOffset, SEEK_SET);
            fread((void*)animations[cnt].images[i].data, sizeof(uint8_t), size * animations[cnt].imageChunk[i].bitsPerPixel / 8, dataFile);
            if (animations[cnt].imageChunk[i].bitsPerPixel == 4)
                for (int32_t j = size / 2 - 1; j >= 0; j--) {
                    uint8_t part1 = animations[cnt].images[i].data[j] >> 4;
                    uint8_t part2 = animations[cnt].images[i].data[j] & 0xF;
                    animations[cnt].images[i].data[j * 2] = part2;
                    animations[cnt].images[i].data[j * 2 + 1] = part1;
                }
        }
        fseek(dataFile, animations[cnt].header.fileSize + animationOffset, SEEK_SET);
    }
    fclose(dataFile);

    //inFileName.replace_extension(".dat2");
    if (0 < fopen_s(&dataFile, inFileName.string().c_str(), "wb") || dataFile == nullptr) {
        printf("Error opening %s for output!\n", inFileName.string().c_str());
        return false;
    }
    fwrite((void*)&count, sizeof(Count), 1, dataFile);
    for (uint32_t cnt = 0; cnt < count.count; cnt++) {
        const uint32_t animationOffset = ftell(dataFile);
        uint32_t dataOffset = 0;
        for (uint32_t i = 0; i < animations[cnt].header.fourthChunkCount; i++) {
            animations[cnt].paletteChunk[i].offset += dataOffset;
            if (animations[cnt].paletteChunk[i].colorCount < 256) {
                animations[cnt].header.fileSize += (256 - animations[cnt].paletteChunk[i].colorCount) * sizeof(Color);
                dataOffset += (256 - animations[cnt].paletteChunk[i].colorCount) * sizeof(Color);
                animations[cnt].paletteChunk[i].colorCount = 256;
            }
        }
        for (uint32_t i = 0; i < animations[cnt].header.fifthChunkCount; i++) {
            animations[cnt].imageChunk[i].offset += dataOffset;
            if (animations[cnt].imageChunk[i].bitsPerPixel == 4) {
                animations[cnt].header.fileSize += animations[cnt].imageChunk[i].width * animations[cnt].imageChunk[i].height * 7 / 2;
                dataOffset += animations[cnt].imageChunk[i].width * animations[cnt].imageChunk[i].height * 7 / 2;
                animations[cnt].imageChunk[i].bitsPerPixel = 8;
            }
            else {
                animations[cnt].header.fileSize += animations[cnt].imageChunk[i].width * animations[cnt].imageChunk[i].height * 3;
                dataOffset += animations[cnt].imageChunk[i].width * animations[cnt].imageChunk[i].height * 3;
            }
            animations[cnt].imageChunk[i].width *= 2;
            animations[cnt].imageChunk[i].height *= 2;
        }
        for (uint32_t i = 0; i < animations[cnt].header.sixthChunkCount; i++) {
            animations[cnt].animationChunk[i].left *= 2;
            animations[cnt].animationChunk[i].top *= 2;
            animations[cnt].animationChunk[i].width *= 2;
            animations[cnt].animationChunk[i].height *= 2;
        }
        for (uint32_t i = 0; i < animations[cnt].header.seventhChunkCount; i++) {
            animations[cnt].frameChunk[i].unk1 *= 2;
            animations[cnt].frameChunk[i].unk2 *= 2;
            animations[cnt].frameChunk[i].offsetX *= 2;
            animations[cnt].frameChunk[i].offsetY *= 2;
            if (animations[cnt].header.index < 3000 || animations[cnt].header.index >= 6000) {
                animations[cnt].frameChunk[i].scaleX /= 2;
                animations[cnt].frameChunk[i].scaleY /= 2;
            }
        }

        fwrite((void*)&animations[cnt].header, sizeof(Header), 1, dataFile);
        fseek(dataFile, animations[cnt].header.firstChunkStart + animationOffset, SEEK_SET);
        fwrite((void*)animations[cnt].firstChunk, sizeof(FirstChunk), animations[cnt].header.firstChunkCount, dataFile);
        fseek(dataFile, animations[cnt].header.secondChunkStart + animationOffset, SEEK_SET);
        fwrite((void*)animations[cnt].actionChunk, sizeof(ActionChunk), animations[cnt].header.secondChunkCount, dataFile);
        fseek(dataFile, animations[cnt].header.thirdChunkStart + animationOffset, SEEK_SET);
        fwrite((void*)animations[cnt].actionFrameChunk, sizeof(ActionFrameChunk), animations[cnt].header.thirdChunkCount, dataFile);
        fseek(dataFile, animations[cnt].header.fourthChunkStart + animationOffset, SEEK_SET);
        fwrite((void*)animations[cnt].paletteChunk, sizeof(FourthChunk), animations[cnt].header.fourthChunkCount, dataFile);
        fseek(dataFile, animations[cnt].header.fifthChunkStart + animationOffset, SEEK_SET);
        fwrite((void*)animations[cnt].imageChunk, sizeof(FifthChunk), animations[cnt].header.fifthChunkCount, dataFile);
        fseek(dataFile, animations[cnt].header.sixthChunkStart + animationOffset, SEEK_SET);
        fwrite((void*)animations[cnt].animationChunk, sizeof(SixthChunk), animations[cnt].header.sixthChunkCount, dataFile);
        fseek(dataFile, animations[cnt].header.seventhChunkStart + animationOffset, SEEK_SET);
        fwrite((void*)animations[cnt].frameChunk, sizeof(SeventhChunk), animations[cnt].header.seventhChunkCount, dataFile);
        fseek(dataFile, animations[cnt].header.eighthChunkStart + animationOffset, SEEK_SET);
        fwrite((void*)animations[cnt].eighthChunk, sizeof(EighthChunk), animations[cnt].header.eighthChunkCount, dataFile);
        fseek(dataFile, animations[cnt].header.ninthChunkStart + animationOffset, SEEK_SET);
        fwrite((void*)animations[cnt].ninthChunk, sizeof(NinthChunk), animations[cnt].header.ninthChunkCount, dataFile);

        for (uint32_t i = 0; i < animations[cnt].header.fourthChunkCount; i++) {
            fseek(dataFile, animations[cnt].paletteChunk[i].offset + animationOffset, SEEK_SET);
            fwrite((void*)animations[cnt].palettes[i].colors, sizeof(Color), animations[cnt].paletteChunk[i].colorCount, dataFile);
        }

        for (uint32_t i = 0; i < animations[cnt].header.fifthChunkCount; i++) {
            const uint32_t size = animations[cnt].imageChunk[i].width * animations[cnt].imageChunk[i].height;
            fseek(dataFile, animations[cnt].imageChunk[i].offset + animationOffset, SEEK_SET);
            for (int32_t y = 0; y < animations[cnt].imageChunk[i].height / 2; y++) {
                for (int32_t x = 0; x < animations[cnt].imageChunk[i].width / 2; x++) {
                    fputc(animations[cnt].images[i].data[y * animations[cnt].imageChunk[i].width / 2 + x], dataFile);
                    fputc(animations[cnt].images[i].data[y * animations[cnt].imageChunk[i].width / 2 + x], dataFile);
                }
                for (int32_t x = 0; x < animations[cnt].imageChunk[i].width / 2; x++) {
                    fputc(animations[cnt].images[i].data[y * animations[cnt].imageChunk[i].width / 2 + x], dataFile);
                    fputc(animations[cnt].images[i].data[y * animations[cnt].imageChunk[i].width / 2 + x], dataFile);
                }
            }
            //fwrite((void*)animations[cnt].images[i].data, sizeof(uint8_t), size, dataFile);
        }
        fseek(dataFile, animations[cnt].header.fileSize + animationOffset, SEEK_SET);
    }

    fclose(dataFile);
    printf("Convert %s!\n", inFileName.string().c_str());
    return true;
}

bool ExtractAnimationNispack(std::filesystem::path inFileName, std::filesystem::path outFileDir) {
    FILE* dataFile, * outFile;
    if (0 < fopen_s(&dataFile, inFileName.string().c_str(), "rb") || dataFile == nullptr) {
        printf("Error opening %s for input!\n", inFileName.string().c_str());
        return false;
    }

    NispackHeader header;
    fread((void*)&header.version, 1, 8, dataFile);
    fread((void*)&header.empty, 1, 4, dataFile);
    fread((void*)&header.entryCount, 1, 4, dataFile);
    header.entries = new NispackHeaderEntry[header.entryCount];

    std::filesystem::path indexFileName(outFileDir);
    indexFileName.replace_filename("index.txt");
    std::ofstream indexFile(indexFileName);
    if (!indexFile.is_open()) {
        printf("Error opening %s for output!\n", indexFileName.string().c_str());
        return false;
    }
    indexFile << header.entryCount << "\n";
    for (uint32_t i = 0; i < header.entryCount; i++) {
        fread((void*)&header.entries[i], 1, 44, dataFile);
        indexFile << header.entries[i].name << " " << header.entries[i].timestamp << "\n";
        std::cout << header.entries[i].name << " " << header.entries[i].timestamp << " " << header.entries[i].size << " " << header.entries[i].start << std::endl;
    }
    indexFile.close();
    for (uint32_t i = 0; i < header.entryCount; i++) {
        std::filesystem::path outFileName(outFileDir);
        outFileName.replace_filename(header.entries[i].name);
        if (0 < fopen_s(&outFile, outFileName.string().c_str(), "wb") || outFile == nullptr) {
            printf("Error opening %s for output!\n", outFileName.string().c_str());
            return false;
        }
        fseek(dataFile, header.entries[i].start, SEEK_SET);
        char* content = new char[header.entries[i].size];
        fread(content, 1, header.entries[i].size, dataFile);
        fwrite(content, 1, header.entries[i].size, outFile);
        fclose(outFile);
        delete[] content;
    }
    fclose(dataFile);

    for (uint32_t i = 0; i < header.entryCount; i++) {
        std::filesystem::path outFileName(outFileDir);
        outFileName.replace_filename(header.entries[i].name);
        if (outFileName.extension() == ".lzs") {
            std::filesystem::path datFileName(outFileName);
            datFileName.replace_extension(".dat");
            if (!DecompressData(outFileName.string().c_str(), datFileName.string().c_str()))
                return false;
            outFileName = datFileName;
        }
        if (!ConvertAnimationDataToHD(outFileName.c_str()))
            return false;
        //if (!ExtractAnimationData(outFileName.c_str()))
        //    return false;
    }
    delete[] header.entries;
    return true;
}