#include <iostream>
#include <fstream>
#include <iomanip>
#include "../shared/MapStructure.h"

bool AppendToMapChr(const char* inFileName, bool writeHeader) {
    FILE* inFile;
    if (0 < fopen_s(&inFile, inFileName, "rb") || inFile == nullptr) {
        printf("Error opening %s for input!\n", inFileName);
        return true;
    }

    std::string fileName(inFileName);
    fileName = fileName.substr(fileName.length() - 11, 7);

    uint16_t recordCount;
    fseek(inFile, 2, SEEK_SET);
    fread((void*)&recordCount, 2, 1, inFile);
    fseek(inFile, 16, SEEK_SET);

    MapChr* data = new MapChr[recordCount];
    for (uint32_t i = 0; i < recordCount; i++) {
        if (sizeof(MapChr) != fread((void*)&data[i], 1, sizeof(MapChr), inFile)) {
            printf("Error reading %s!\n", inFileName);
            return false;
        }
    }
    uint8_t extra[16];
    for (uint32_t i = 0; i < 16; i++) {
        if (sizeof(uint8_t) != fread((void*)&extra[i], 1, sizeof(uint8_t), inFile)) {
            printf("Error reading %s extra!\n", inFileName);
            return false;
        }
    }

    std::ofstream outFile("../extracted/txt/mapChr.txt", writeHeader ? std::ios_base::out : std::ios_base::app);
    if (!outFile.is_open()) {
        printf("Error opening ../extracted/txt/mapChr.txt for output!\n");
        return false;
    }
    if (writeHeader) {
        const char* size = 
            "0\t0\t2\t2\t2\t2\t2\t2\t"
            "1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t"
            "2\t2\t2\t2\t2\t2\t2\t" //general
            "1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t"
            "1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t"
            "1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t"
            "1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t"
            "1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t"
            "2\t1\t1\t1\t1\t1\t1\t2\t1\t1\t1\t1\t1\t1\t" //items
            "2\t1\t1\t1\t1\t1\t1\t2\t1\t1\t1\t1\t1\t1\t" //items
            "1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t"
            "1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t"
            "1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t"
            "1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1";
        const char* header =
            "file\trow\tcharRow\tlevel\tfacing\tposX\tposZ\tposY\tcontrol\t"
            "aiFirst\taiAfter\tunk15\tappear\tunk17\tunk18\tunk19\tunk20\tempty21\t"
            "geoEffect\tgeoColor\tunk26\tunk28\tunk30\tunk32\tunk34\t"
            "unk36\tunk37\tunk38\tunk39\tunk40\tunk41\tunk42\tunk43\tunk44\tunk45\tunk46\tunk47\tunk48\tunk49\tunk50\tunk51\t"
            "unk52\tunk53\tunk54\tunk55\tunk56\tunk57\tunk58\tunk59\tunk60\tunk61\tunk62\tunk63\tunk64\tunk65\tunk66\tunk67\t"
            "unk68\tunk69\tunk70\tunk71\tunk72\tunk73\tunk74\tunk75\tunk76\tunk77\tunk78\tunk79\tunk80\tunk81\tunk82\tunk83\t"
            "unk84\tunk85\tunk86\tunk87\tunk88\tunk89\tunk90\tunk91\tunk92\tunk93\tunk94\tunk95\tunk96\tunk97\tunk98\tunk99\t"
            "unk100\tunk101\tunk102\tunk103\tunk104\tunk105\tunk106\tunk107\tunk108\tunk109\tunk110\tunk111\tunk112\tunk113\tunk114\tunk115\t"
            "item1id\tempty118\tunk119\tunk120\tunk121\tunk122\tunk123\titem2id\tempty126\tunk127\tunk128\tunk129\tunk130\tunk131\t"
            "item3id\tempty134\tunk135\tunk136\tunk137\tunk138\tunk139\titem4id\tempty142\tunk143\tunk144\tunk145\tunk146\tunk147\t"
            "unk148\tunk149\tunk150\tunk151\tunk152\tunk153\tunk154\tunk155\tunk156\tunk157\tunk158\t"
            "unk159\tunk160\tunk161\tunk162\tunk163\tunk164\tunk165\tunk166\tunk167\tunk168\tunk169\t"
            "unk170\tunk171\tunk172\tunk173\tunk174\tunk175\tunk176\tunk177\tunk178\tunk179\tunk180\t"
            "unk181\tunk182\tunk183\tunk184\tunk185\tunk186\tunk187\tunk188\tunk189\tunk190\tunk191\t";
        outFile << size << "\n" << header << "\n";
    }
    for (uint32_t i = 0; i < recordCount; i++)
        outFile << fileName << "\t" << i << "\t" << data[i].charRow << "\t" << data[i].level << "\t" << data[i].facing << "\t"
        << data[i].posX << "\t" << data[i].posZ << "\t" << data[i].posY << "\t" << (uint32_t)data[i].control << "\t"
        << (uint32_t)data[i].aiFirst << "\t" << (uint32_t)data[i].aiAfter << "\t" << (uint32_t)data[i].unk15 << "\t" << (uint32_t)data[i].appear << "\t"
        << (uint32_t)data[i].unk17 << "\t" << (uint32_t)data[i].unk18 << "\t" << (uint32_t)data[i].unk19 << "\t" << (uint32_t)data[i].unk20 << "\t" << (uint32_t)data[i].empty21 << "\t"
        << data[i].geoEffect << "\t" << data[i].geoColor << "\t" << data[i].unk26 << "\t" << data[i].unk28 << "\t" << data[i].unk30 << "\t" << data[i].unk32 << "\t" << data[i].unk34 << "\t"
        << (uint32_t)data[i].unk36 << "\t" << (uint32_t)data[i].unk37 << "\t" << (uint32_t)data[i].unk38 << "\t" << (uint32_t)data[i].unk39 << "\t"
        << (uint32_t)data[i].unk40 << "\t" << (uint32_t)data[i].unk41 << "\t" << (uint32_t)data[i].unk42 << "\t" << (uint32_t)data[i].empty43 << "\t"
        << (uint32_t)data[i].unk44 << "\t" << (uint32_t)data[i].unk45 << "\t" << (uint32_t)data[i].unk46 << "\t" << (uint32_t)data[i].unk47 << "\t"
        << (uint32_t)data[i].unk48 << "\t" << (uint32_t)data[i].unk49 << "\t" << (uint32_t)data[i].unk50 << "\t" << (uint32_t)data[i].unk51 << "\t"
        << (uint32_t)data[i].unk52 << "\t" << (uint32_t)data[i].unk53 << "\t" << (uint32_t)data[i].unk54 << "\t" << (uint32_t)data[i].unk55 << "\t"
        << (uint32_t)data[i].unk56 << "\t" << (uint32_t)data[i].unk57 << "\t" << (uint32_t)data[i].unk58 << "\t" << (uint32_t)data[i].unk59 << "\t"
        << (uint32_t)data[i].unk60 << "\t" << (uint32_t)data[i].unk61 << "\t" << (uint32_t)data[i].unk62 << "\t" << (uint32_t)data[i].unk63 << "\t"
        << (uint32_t)data[i].unk64 << "\t" << (uint32_t)data[i].unk65 << "\t" << (uint32_t)data[i].unk66 << "\t" << (uint32_t)data[i].unk67 << "\t"
        << (uint32_t)data[i].unk68 << "\t" << (uint32_t)data[i].unk69 << "\t" << (uint32_t)data[i].unk70 << "\t" << (uint32_t)data[i].unk71 << "\t"
        << (uint32_t)data[i].unk72 << "\t" << (uint32_t)data[i].unk73 << "\t" << (uint32_t)data[i].unk74 << "\t" << (uint32_t)data[i].unk75 << "\t"
        << (uint32_t)data[i].unk76 << "\t" << (uint32_t)data[i].unk77 << "\t" << (uint32_t)data[i].unk78 << "\t" << (uint32_t)data[i].unk79 << "\t"
        << (uint32_t)data[i].unk80 << "\t" << (uint32_t)data[i].unk81 << "\t" << (uint32_t)data[i].unk82 << "\t" << (uint32_t)data[i].unk83 << "\t"
        << (uint32_t)data[i].unk84 << "\t" << (uint32_t)data[i].unk85 << "\t" << (uint32_t)data[i].unk86 << "\t" << (uint32_t)data[i].unk87 << "\t"
        << (uint32_t)data[i].unk88 << "\t" << (uint32_t)data[i].unk89 << "\t" << (uint32_t)data[i].unk90 << "\t" << (uint32_t)data[i].unk91 << "\t"
        << (uint32_t)data[i].unk92 << "\t" << (uint32_t)data[i].unk93 << "\t" << (uint32_t)data[i].unk94 << "\t" << (uint32_t)data[i].unk95 << "\t"
        << (uint32_t)data[i].unk96 << "\t" << (uint32_t)data[i].unk97 << "\t" << (uint32_t)data[i].unk98 << "\t" << (uint32_t)data[i].unk99 << "\t"
        << (uint32_t)data[i].unk100 << "\t" << (uint32_t)data[i].unk101 << "\t" << (uint32_t)data[i].unk102 << "\t" << (uint32_t)data[i].unk103 << "\t"
        << (uint32_t)data[i].unk104 << "\t" << (uint32_t)data[i].unk105 << "\t" << (uint32_t)data[i].unk106 << "\t" << (uint32_t)data[i].unk107 << "\t"
        << (uint32_t)data[i].unk108 << "\t" << (uint32_t)data[i].unk109 << "\t" << (uint32_t)data[i].unk110 << "\t" << (uint32_t)data[i].unk111 << "\t"
        << (uint32_t)data[i].unk112 << "\t" << (uint32_t)data[i].unk113 << "\t" << (uint32_t)data[i].unk114 << "\t" << (uint32_t)data[i].unk115 << "\t"
        << data[i].item1Id << "\t" << (uint32_t)data[i].empty118 << "\t" << (uint32_t)data[i].unk119 << "\t" << (uint32_t)data[i].unk120 << "\t" << (uint32_t)data[i].unk121 << "\t" << (uint32_t)data[i].unk122 << "\t" << (uint32_t)data[i].unk123 << "\t"
        << data[i].item2Id << "\t" << (uint32_t)data[i].empty126 << "\t" << (uint32_t)data[i].unk127 << "\t" << (uint32_t)data[i].unk128 << "\t" << (uint32_t)data[i].unk129 << "\t" << (uint32_t)data[i].unk130 << "\t" << (uint32_t)data[i].unk131 << "\t"
        << data[i].item3Id << "\t" << (uint32_t)data[i].empty134 << "\t" << (uint32_t)data[i].unk135 << "\t" << (uint32_t)data[i].unk136 << "\t" << (uint32_t)data[i].unk137 << "\t" << (uint32_t)data[i].unk138 << "\t" << (uint32_t)data[i].unk139 << "\t"
        << data[i].item4Id << "\t" << (uint32_t)data[i].empty142 << "\t" << (uint32_t)data[i].unk143 << "\t" << (uint32_t)data[i].unk144 << "\t" << (uint32_t)data[i].unk145 << "\t" << (uint32_t)data[i].unk146 << "\t" << (uint32_t)data[i].unk147 << "\t"
        << (uint32_t)data[i].unk148 << "\t" << (uint32_t)data[i].unk149 << "\t" << (uint32_t)data[i].unk150 << "\t" << (uint32_t)data[i].unk151 << "\t"
        << (uint32_t)data[i].unk152 << "\t" << (uint32_t)data[i].unk153 << "\t" << (uint32_t)data[i].unk154 << "\t" << (uint32_t)data[i].unk155 << "\t"
        << (uint32_t)data[i].unk156 << "\t" << (uint32_t)data[i].unk157 << "\t" << (uint32_t)data[i].unk158 << "\t" << (uint32_t)data[i].unk159 << "\t"
        << (uint32_t)data[i].unk160 << "\t" << (uint32_t)data[i].unk161 << "\t" << (uint32_t)data[i].unk162 << "\t" << (uint32_t)data[i].unk163 << "\t"
        << (uint32_t)data[i].unk164 << "\t" << (uint32_t)data[i].unk165 << "\t" << (uint32_t)data[i].unk166 << "\t" << (uint32_t)data[i].unk167 << "\t"
        << (uint32_t)data[i].unk168 << "\t" << (uint32_t)data[i].unk169 << "\t" << (uint32_t)data[i].unk170 << "\t" << (uint32_t)data[i].unk171 << "\t"
        << (uint32_t)data[i].unk172 << "\t" << (uint32_t)data[i].unk173 << "\t" << (uint32_t)data[i].unk174 << "\t" << (uint32_t)data[i].unk175 << "\t"
        << (uint32_t)data[i].unk176 << "\t" << (uint32_t)data[i].unk177 << "\t" << (uint32_t)data[i].unk178 << "\t" << (uint32_t)data[i].unk179 << "\t"
        << (uint32_t)data[i].unk180 << "\t" << (uint32_t)data[i].unk181 << "\t" << (uint32_t)data[i].unk182 << "\t" << (uint32_t)data[i].unk183 << "\t"
        << (uint32_t)data[i].unk184 << "\t" << (uint32_t)data[i].unk185 << "\t" << (uint32_t)data[i].unk186 << "\t" << (uint32_t)data[i].unk187 << "\t"
        << (uint32_t)data[i].unk188 << "\t" << (uint32_t)data[i].unk189 << "\t" << (uint32_t)data[i].unk190 << "\t" << (uint32_t)data[i].unk191 << "\n";
    outFile.close();

    std::ofstream outFile2("../extracted/txt/mapChrExtra.txt", writeHeader ? std::ios_base::out : std::ios_base::app);
    if (!outFile2.is_open()) {
        printf("Error opening ../extracted/txt/mapChrExtra.txt for output!\n");
        return false;
    }
    if (writeHeader) {
        const char* size =
            "0\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1\t1";
        const char* header =
            "file\tunk1\tunk2\tunk3\tunk4\tunk5\tunk6\tunk7\tunk8\tunk9\tunk10\tunk11\tunk12\tunk13\tunk14\tunk15\tunk16";
        outFile2 << size << "\n" << header << "\n";
    }
    outFile2 << fileName << "\t";
    for (uint32_t i = 0; i < 15; i++)
        outFile2 << (uint32_t)extra[i] << "\t";
    outFile2 << (uint32_t)extra[15] << "\n";
    outFile2.close();
    return true;
}

bool AppendToMapPos(const char* inFileName, bool writeHeader) {
    FILE* inFile;
    if (0 < fopen_s(&inFile, inFileName, "rb") || inFile == nullptr) {
        printf("Error opening %s for input!\n", inFileName);
        return true;
    }

    std::string fileName(inFileName);
    fileName = fileName.substr(fileName.length() - 11, 7);

    uint16_t recordCount;
    fseek(inFile, 2, SEEK_SET);
    fread((void*)&recordCount, 2, 1, inFile);
    fseek(inFile, 16, SEEK_SET);

    MapPos* data = new MapPos[recordCount];
    for (uint32_t i = 0; i < recordCount; i++) {
        if (sizeof(MapPos) != fread((void*)&data[i], 1, sizeof(MapPos), inFile)) {
            printf("Error reading %s!\n", inFileName);
            return false;
        }
    }

    std::ofstream outFile("../extracted/txt/mapPos.txt", writeHeader ? std::ios_base::out : std::ios_base::app);
    if (!outFile.is_open()) {
        printf("Error opening ../extracted/txt/mapPos.txt for output!\n");
        return false;
    }
    if (writeHeader) {
        const char* size = "0\t0\t1\t1\t1\t1\t1\t1\t1\t1";
        const char* header = "file\tline\ttype\tposX\tposY\tempty1\tunk\tgeoColor\tempty2\tempty3";
        outFile << size << "\n" << header << "\n";
    }
    for (uint32_t i = 0; i < recordCount; i++)
        outFile << fileName << "\t" << i << "\t" << (uint32_t)data[i].type << "\t" << (uint32_t)data[i].posX << "\t" << (uint32_t)data[i].posY << "\t" << (uint32_t)data[i].empty1 << "\t"
            << (uint32_t)data[i].unk << "\t" << (uint32_t)data[i].geoColor << "\t" << (uint32_t)data[i].empty2 << "\t" << (uint32_t)data[i].empty3 << "\n";
    outFile.close();
    return true;
}