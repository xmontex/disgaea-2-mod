#include<stdio.h>
#include<stdlib.h>
#include <iostream>

struct LzsHeader {
    char extension[4];
    uint32_t decompressedSize;
    uint32_t compressedSize;
    unsigned char magicChar;
    char padding[3];
};

bool DecompressData(const char* inFileName, const char* outFileName) {
    FILE *inFile, *outFile;
    unsigned char *src, *dest, *srcStart, *destStart;
    LzsHeader header;
    
    if (0 < fopen_s(&inFile, inFileName, "rb") || inFile == nullptr) {
        printf("Error opening %s for input!\n", inFileName);
        return false;
    }
    if (0 < fopen_s(&outFile, outFileName, "wb") || outFile == nullptr) {
        printf("Error opening %s for output!\n", outFileName);
        return false;
    }

    fseek(inFile, 0, SEEK_END);
    size_t filesize = ftell(inFile);
    fseek(inFile, 0, SEEK_SET);
    if (filesize < 16) {
        fclose(inFile);
        fclose(outFile);
        return true;
    }

    fread((void *)&header, 16, 1, inFile);
    header.compressedSize -= 12;
    destStart = dest = new unsigned char[header.decompressedSize];
    srcStart = src = new unsigned char[header.compressedSize];
    fread(src, 1, header.compressedSize, inFile);

    uint8_t length;
    uint8_t offset;
    while (src < srcStart + header.compressedSize) {
        if (*src == header.magicChar) {
            src++;
            if (*src == header.magicChar)
                *dest++ = *src++;
            else {
                offset = *src++;
                if (offset > header.magicChar)
                    offset--;
                length = *src++;
                for (uint8_t i = 0; i < length; i++) {
                    *dest = *(dest - offset);
                    dest++;
                }
            }
        }
        else
            *dest++ = *src++;
    }

    fwrite(destStart, 1, header.decompressedSize, outFile);

    delete[] srcStart;
    delete[] destStart;
    fclose(inFile);
    fclose(outFile);
    return true;
}