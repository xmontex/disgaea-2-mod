#pragma once
#include <filesystem>
bool ExtractAnimationNispack(std::filesystem::path inFileName, std::filesystem::path outFileDir);
bool ExtractAnimationData(std::filesystem::path inFileName);
bool ConvertAnimationDataToHD(std::filesystem::path inFileName);