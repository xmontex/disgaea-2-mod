#pragma once
#include <string>

struct DataHeaderEntry {
    char name[40] = "";
    uint32_t unk;
    uint32_t size;
    uint32_t start;
};

struct DataHeader {
    char version[8] = "";
    uint32_t entryCount;
    char empty[4] = "";
    DataHeaderEntry* entries;
};

struct StartDataHeaderEntry {
    uint32_t end;
    char name[28] = "";
};

struct StartDataHeader {
    uint32_t entryCount;
    char empty[12] = "";
    StartDataHeaderEntry* entries;
};

struct NispackHeaderEntry {
    char name[32] = "";
    uint32_t start;
    uint32_t size;
    uint32_t timestamp;
};

struct NispackHeader {
    char version[8] = "";
    char empty[4] = "";
    uint32_t entryCount;
    NispackHeaderEntry* entries;
};

struct DsarcHeaderEntry {
    char name[40] = "";
    uint32_t size;
    uint32_t start;
};

struct DsarcHeader {
    char version[8] = "";
    uint32_t entryCount;
    char empty[4] = "";
    DsarcHeaderEntry* entries;
};