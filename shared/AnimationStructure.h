#pragma once
#include <string>
#include <windows.h>

const bool useAnimation = false;

struct Count {
    uint32_t count;
    uint32_t empty[3];
};

struct Header {
    uint32_t fileSize;
    uint16_t index;
    uint16_t firstChunkCount;
    uint16_t secondChunkCount;
    uint16_t thirdChunkCount;
    uint16_t fourthChunkCount;
    uint16_t fifthChunkCount;
    uint16_t sixthChunkCount;
    uint16_t seventhChunkCount;
    uint16_t eighthChunkCount;
    uint16_t ninthChunkCount;
    uint32_t firstChunkStart;
    uint32_t secondChunkStart;
    uint32_t thirdChunkStart;
    uint32_t fourthChunkStart;
    uint32_t fifthChunkStart;
    uint32_t sixthChunkStart;
    uint32_t seventhChunkStart;
    uint32_t eighthChunkStart;
    uint32_t ninthChunkStart;
};

struct FirstChunk {
    uint16_t offset;
    uint16_t start;
    uint16_t end;
    uint16_t empty;
};

struct ActionChunk {
    uint8_t unk1;
    uint8_t unk2;
    uint8_t unk3;
    uint8_t unk4;
    int16_t thirdChunkId;
    int16_t thirdChunkCount;
    int16_t unk7;
    int16_t unk8;
};

struct ActionFrameChunk {
    uint16_t unk1;
    uint16_t unk2;
    uint16_t sixthId;
    uint16_t seventhId;
    uint16_t unk5;
    uint16_t unk6;
};

struct FourthChunk { //colormap
    uint32_t offset;
    uint32_t colorCount;
};

struct FifthChunk { //image
    uint32_t offset;
    uint16_t width;
    uint16_t height;
    uint8_t bitsPerPixel;
    uint8_t unk1;
    uint8_t unk2;
    uint8_t empty;
};

struct SixthChunk { //animation
    int16_t unk1;
    int16_t unk2;
    int8_t unk3;
    int8_t unk4;
    int8_t unk5;
    int8_t unk6;
    uint16_t left;
    uint16_t top;
    uint16_t width;
    uint16_t height;
};

struct SeventhChunk { //frames
    int16_t unk1;
    int16_t unk2;
    int16_t offsetX;
    int16_t offsetY;
    int16_t scaleX;
    int16_t scaleY;
    int32_t unk3;
};

struct EighthChunk {
    int16_t unk1;
    int16_t unk2;
    int16_t unk3;
    int16_t unk4;
    int16_t unk5;
    int16_t unk6;
    int16_t unk7;
    int16_t unk8;
};

struct NinthChunk {
    int32_t unk1;
};

struct Color {
    uint8_t r;
    uint8_t g;
    uint8_t b;
    uint8_t a;
};

struct Palette {
    Color* colors;
};

struct Image {
    uint8_t* data;
};

struct Animation {
    Header header;
    FirstChunk* firstChunk;
    ActionChunk* actionChunk;
    ActionFrameChunk* actionFrameChunk;
    FourthChunk* paletteChunk;
    FifthChunk* imageChunk;
    SixthChunk* animationChunk;
    SeventhChunk* frameChunk;
    EighthChunk* eighthChunk;
    NinthChunk* ninthChunk;
    Palette* palettes;
    Image* images;
};

struct BmpHeader {
    BITMAPFILEHEADER fileHeader;
    BITMAPINFOHEADER infoHeader;
    RGBQUAD colors[256];
};