#pragma once
#include <string>

struct character { //352 bytes
    char name[52] = "";
    char title[69] = "";
    uint8_t gender;
    uint8_t spellCount;
    uint8_t tier;
    uint8_t unk1;
    uint8_t jumpHeight;
    uint8_t profFists;
    uint8_t profSword;
    uint8_t profSpear;
    uint8_t profBow;
    uint8_t profGun;
    uint8_t profAxe;
    uint8_t profStaff;
    uint8_t unk2;
    uint8_t specAbility;
    uint8_t aptHP;
    uint8_t aptSP;
    uint8_t aptATK;
    uint8_t aptDEF;
    uint8_t aptINT;
    uint8_t aptSPD;
    uint8_t aptHIT;
    uint8_t aptRES;
    uint8_t unk3;
    uint8_t type;
    uint8_t throwDist;
    uint8_t counter;
    uint8_t unk4;
    uint8_t unk5;
    uint8_t unk6;
    uint16_t id;
    uint16_t first;
    uint16_t unk7;
    uint16_t unk8;
    uint16_t desc;
    uint16_t statHP;
    uint16_t statSP;
    uint16_t statATK;
    uint16_t statDEF;
    uint16_t statINT;
    uint16_t statSPD;
    uint16_t statHIT;
    uint16_t statRES;
    uint16_t expRate;
    uint16_t moveDist;
    uint16_t canFly;
    uint16_t attackRange;
    uint16_t killHell;
    uint16_t killExp;
    uint16_t unk9;
    uint16_t createMana;
    uint16_t spellId[32];
    uint16_t spellLearnedAt[32];
    uint16_t id2;
    uint16_t empty[15];
};

struct characterhelp { //288 bytes
    char line1[92] = "";
    char line2[92] = "";
    char line3[92] = "";
    uint32_t id;
    uint32_t id2;
    uint32_t empty;
};


struct darksun1 { //96 bytes
    char name[44] = "";
    uint8_t unk1;
    uint8_t unk2;
    uint8_t group[8];
    uint16_t id;
    uint16_t unk3;
    uint16_t target;
    uint16_t id2;
    uint16_t empty[17];
};

struct darksun2 { //96 bytes
    char name[52] = "";
    uint8_t unk1;
    uint8_t group[8];
    uint8_t unk2;
    uint16_t id;
    uint16_t unk3;
    uint16_t target;
    uint16_t id2;
    uint16_t empty[13];
};

struct dktable { //164
    uint16_t mapId;
    uint8_t count;
    uint8_t difficulty;
    uint8_t dkGroup[32];
    uint16_t dkSun[32];
    uint8_t dkEffect[32];
    uint8_t dkEmpty[32];
};

struct dungeon { //128 bytes
    char name[52] = "";
    uint32_t id;
    uint32_t dwTarget;
    uint32_t empty[3];
    uint16_t unk1;
    uint16_t bonusRank;
    uint16_t dwCondition;
    uint16_t empty2;
    uint8_t dwUnlock;
    uint8_t removeUnique1;
    uint8_t empty3;
    uint8_t dwFlag;
    uint8_t removeUnique2;
    uint8_t empty4[23];
    uint16_t dwMap;
    uint16_t dwLevel;
    uint32_t id2;
    uint32_t empty5[3];
};

struct judgment { //128 bytes
    char name[68] = "";
    uint8_t id;
    uint8_t worldLevel;
    uint8_t felonyCount;
    uint8_t goalType;
    uint8_t rewardType;
    uint8_t empty[19];
    uint32_t goal;
    uint32_t reward;
    uint8_t id2;
    uint8_t empty2[27];
};

struct hospital { //16 bytes
    uint16_t id;
    uint16_t revive;
    uint32_t reward;
    uint32_t restoreHP;
    uint32_t restoreSP;
};

struct mitem { //256 bytes
    char name[68] = "";
    char desc[136] = "";
    uint8_t statid;
    int8_t statvalue;
    uint8_t rank;
    uint8_t range;
    uint8_t jump;
    uint8_t unk4;
    uint8_t critChance;
    uint8_t scrollSkill;
    uint16_t type;
    int16_t statHP;
    int16_t statSP;
    int16_t statATK;
    int16_t statDEF;
    int16_t statINT;
    int16_t statSPD;
    int16_t statHIT;
    int16_t statRES;
    uint16_t id;
    uint16_t icon;
    uint16_t type2;
    uint16_t move;
    uint16_t specialist;
    uint16_t sprite;
    uint16_t unk;
    uint32_t price;
    uint16_t id2;
    uint16_t empty[3];
};

struct pirate { //160 bytes
    char name[44] = "";
    uint16_t id;
    uint8_t worldLevel;
    uint8_t specialists;
    uint8_t treasures;
    uint8_t unk1;
    uint8_t crewSize;
    uint8_t shipType;
    uint8_t chanceRecovery;
    uint8_t chanceFist;
    uint8_t chanceSword;
    uint8_t chanceSpear;
    uint8_t chanceBow;
    uint8_t chanceGun;
    uint8_t chanceAxe;
    uint8_t chanceStaff;
    uint8_t chanceMeleeClaw;
    uint8_t chanceCasterClaw;
    uint8_t chanceArmor;
    uint8_t chanceBelt;
    uint8_t chanceShoes;
    uint8_t chanceOrb;
    uint8_t chanceGlasses;
    uint8_t chanceMuscle;
    uint8_t chanceWeight;
    uint8_t chanceMap;
    uint8_t chanceEmblem;
    uint8_t chanceLegendary;
    uint8_t unk2;
    uint8_t unk3;
    uint8_t unk4;
    uint8_t unk5;
    uint16_t pirateId;
    uint16_t pirateLvlMult;
    uint16_t crewId[16];
    uint16_t crewLvlMult[16];
    uint16_t id2;
    uint16_t empty[7];
};

struct habit { //128 bytes
    char name[68] = "";
    uint8_t chanceRecovery;
    uint8_t chanceFist;
    uint8_t chanceSword;
    uint8_t chanceSpear;
    uint8_t chanceBow;
    uint8_t chanceGun;
    uint8_t chanceAxe;
    uint8_t chanceStaff;
    uint8_t chanceMeleeClaw;
    uint8_t chanceCasterClaw;
    uint8_t chanceArmor;
    uint8_t chanceBelt;
    uint8_t chanceShoes;
    uint8_t chanceOrb;
    uint8_t chanceGlasses;
    uint8_t chanceMuscle;
    uint8_t chanceWeight;
    uint8_t chanceMap;
    uint8_t chanceEmblem;
    uint8_t chanceLegendary;
    uint8_t unk21;
    uint8_t unk22;
    uint16_t id;
    uint32_t maxStack;
    uint16_t id2;
    uint16_t empty[15];
};

struct magic { //256 bytes
    uint16_t id;
    uint16_t unk1;
    uint16_t weaponLvl;
    uint16_t unk2;
    uint16_t cost;
    char name[52] = "";
    char desc[148] = "";
    int8_t effectValue[6];
    uint8_t weaponType;
    uint8_t element;
    uint8_t spellType;
    uint8_t weaponType2;
    uint8_t unk3;
    uint8_t targetRange;
    uint8_t targetInLine;
    uint8_t targetShape;
    uint8_t minHeight;
    uint8_t maxHeight;
    uint8_t effectType[6];
    uint16_t id2;
    uint16_t empty[11];
};

struct wish { //256 bytes
    uint16_t cost;
    uint16_t unk1;
    uint16_t id;
    uint16_t senators;
    uint8_t legendary;
    int8_t approval;
    char name[68] = "";
    char desc[126] = "";
    uint16_t id2;
    uint16_t empty[25];
};

struct wish2 { //320 bytes
    uint32_t unk1;
    uint32_t unk2;
    uint32_t unk3;
    uint16_t proposerId;
    uint16_t empty[8];
    int16_t unk4;
    uint8_t id;
    uint8_t group;
    uint8_t unk5;
    uint8_t empty2[2];
    uint8_t unk6;
    char name[104] = "";
    char desc[126] = "";
    uint8_t id2;
    uint8_t empty3[51];
};