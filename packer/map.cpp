#include <inttypes.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <stdio.h>
#include "map.h"
#include "../shared/MapStructure.h"
#include "../shared/DataStructure.h"

bool hasEnding(std::string const& fullString, std::string const& ending) {
    if (fullString.length() >= ending.length()) {
        return (0 == fullString.compare(fullString.length() - ending.length(), ending.length(), ending));
    }
    else {
        return false;
    }
}

FILE* OpenFile(std::string charMap, std::string extension) {
    FILE* outFile = nullptr;
    std::string indexFileName("../extracted/data/mappack/" + charMap + "/index.txt");
    std::ifstream indexFile(indexFileName);
    if (!indexFile.is_open()) {
        printf("Error opening %s for input!\n", indexFileName.c_str());
        return outFile;
    }
    std::string token;
    std::getline(indexFile, token);
    int entryCount = std::atoi(token.c_str());
    for (int i = 0; i < entryCount; i++) {
        std::getline(indexFile, token);
        if (hasEnding(token, extension))
            break;
    }

    std::string newFilePath = "../extracted/data/mappack/" + charMap + "/" + token + "2";
    if (0 < fopen_s(&outFile, newFilePath.c_str(), "wb") || outFile == nullptr) {
        printf("Error opening %s for output!\n", newFilePath.c_str());
        return outFile;
    }

    int placeholder = 0;
    fwrite((void*)&placeholder, 4, 1, outFile);
    fwrite((void*)&placeholder, 4, 1, outFile);
    fwrite((void*)&placeholder, 4, 1, outFile);
    fwrite((void*)&placeholder, 4, 1, outFile);
    return outFile;
}

void CloseFile(FILE* outFile, uint16_t recordCount, std::string lineExtra, std::string currentMap) {
    //extra unknown data to chatacter file
    if (lineExtra != "") {
        std::string token;
        std::stringstream streamExtra(lineExtra);
        std::getline(streamExtra, token, '\t');
        if (token != currentMap) {
            printf("Data mismatch: data=%s, extra=%s!\n", currentMap.c_str(), token.c_str());
        }
        else {
            uint8_t extra;
            for (int i = 0; i < 16; i++) {
                std::getline(streamExtra, token, '\t');
                extra = std::atoi(token.c_str());
                fwrite((void*)&extra, sizeof(uint8_t), 1, outFile);
            }
        }
    }

    //padding
    size_t size = ftell(outFile);
    if (size % 16 != 0) {
        size = (size / 16 + 1) * 16 - 1;
        fseek(outFile, size, SEEK_SET);
        fputc(0, outFile);
    }

    //proper record count
    fseek(outFile, 2, SEEK_SET);
    fwrite((void*)&recordCount, 2, 1, outFile);

    fclose(outFile);
}

bool ReadTxtMapChr() {
    printf("Packing map character files... ");
    std::ifstream inFile("../extracted/txt/mapChr.txt");
    if (!inFile.is_open()) {
        printf("Error opening ../extracted/txt/mapChr.txt for input!\n");
        return false;
    }

    std::ifstream inFileExtra("../extracted/txt/mapChrExtra.txt");
    if (!inFile.is_open()) {
        printf("Error opening ../extracted/txt/mapChrExtra.txt for input!\n");
        return false;
    }

    std::string line;
    std::getline(inFile, line);
    std::getline(inFile, line);

    std::string lineExtra;
    std::getline(inFileExtra, lineExtra);
    std::getline(inFileExtra, lineExtra);

    FILE* outFile = nullptr;
    MapChr data;
    uint16_t recordCount = 0;
    std::string currentMap = "";
    while (std::getline(inFile, line)) {
        std::stringstream stream(line);
        std::string token;
        std::getline(stream, token, '\t');
        std::string charMap = token.c_str();
        std::getline(stream, token, '\t'); // row - skip
        std::getline(stream, token, '\t');
        data.charRow = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.level = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.facing = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.posX = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.posZ = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.posY = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.control = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.aiFirst = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.aiAfter = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk15 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.appear = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk17 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk18 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk19 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk20 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.empty21 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.geoEffect = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.geoColor = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk26 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk28 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk30 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk32 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk34 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk36 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk37 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk38 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk39 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk40 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk41 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk42 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.empty43 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk44 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk45 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk46 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk47 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk48 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk49 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk50 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk51 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk52 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk53 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk54 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk55 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk56 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk57 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk58 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk59 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk60 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk61 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk62 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk63 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk64 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk65 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk66 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk67 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk68 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk69 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk70 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk71 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk72 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk73 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk74 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk75 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk76 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk77 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk78 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk79 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk80 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk81 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk82 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk83 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk84 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk85 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk86 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk87 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk88 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk89 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk90 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk91 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk92 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk93 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk94 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk95 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk96 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk97 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk98 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk99 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk100 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk101 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk102 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk103 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk104 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk105 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk106 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk107 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk108 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk109 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk110 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk111 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk112 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk113 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk114 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk115 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.item1Id = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.empty118 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk119 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk120 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk121 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk122 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk123 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.item2Id = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.empty126 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk127 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk128 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk129 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk130 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk131 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.item3Id = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.empty134 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk135 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk136 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk137 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk138 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk139 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.item4Id = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.empty142 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk143 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk144 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk145 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk146 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk147 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk148 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk149 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk150 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk151 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk152 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk153 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk154 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk155 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk156 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk157 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk158 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk159 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk160 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk161 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk162 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk163 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk164 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk165 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk166 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk167 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk168 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk169 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk170 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk171 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk172 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk173 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk174 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk175 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk176 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk177 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk178 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk179 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk180 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk181 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk182 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk183 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk184 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk185 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk186 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk187 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk188 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk189 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk190 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk191 = std::atoi(token.c_str());

        if (charMap != currentMap)
        {
            if (outFile != nullptr) {
                std::getline(inFileExtra, lineExtra);
                CloseFile(outFile, recordCount, lineExtra, currentMap);
            }

            recordCount = 0;
            outFile = OpenFile(charMap, ".chr");
            if (outFile == nullptr)
                continue;
            currentMap = charMap.c_str();
        }

        recordCount++;
        fwrite((void*)&data, sizeof(data), 1, outFile);
        data = MapChr(); //reset is required
    }

    std::getline(inFileExtra, lineExtra);
    CloseFile(outFile, recordCount, lineExtra, currentMap);
    inFile.close();
    printf("Done!\n");
    return true;
}

bool ReadTxtMapPos() {
    printf("Packing map positions files... ");
    std::ifstream inFile("../extracted/txt/mapPos.txt");
    if (!inFile.is_open()) {
        printf("Error opening ../extracted/txt/mapPos.txt for input!\n");
        return false;
    }

    FILE* outFile = nullptr;

    std::string line;
    std::getline(inFile, line);
    std::getline(inFile, line);

    MapPos data;
    uint16_t recordCount = 0;
    std::string currentMap = "";
    while (std::getline(inFile, line)) {
        std::stringstream stream(line);
        std::string token;
        std::getline(stream, token, '\t');
        std::string charMap = token.c_str();
        std::getline(stream, token, '\t'); // row - skip
        std::getline(stream, token, '\t');
        data.type = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.posX = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.posY = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.empty1 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.geoColor = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.empty2 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.empty3 = std::atoi(token.c_str());
        
        if (charMap != currentMap)
        {
            if (outFile != nullptr)
                CloseFile(outFile, recordCount, "", "");

            recordCount = 0;
            outFile = OpenFile(charMap, ".pos");
            if (outFile == nullptr)
                continue;
            currentMap = charMap.c_str();
        }

        recordCount++;
        fwrite((void*)&data, sizeof(data), 1, outFile);
        data = MapPos(); //reset is required
    }

    CloseFile(outFile, recordCount, "", "");
    inFile.close();
    printf("Done!\n");
    return true;
}

bool ReadMapFiles() {
    if (!ReadTxtMapChr())
        return false;
    if (!ReadTxtMapPos())
        return false;
    return true;
}