#include <iostream>
#include <fstream>
#include "StartData.h"
#include "map.h"
#include "lzs.h"
#include "../shared/DataStructure.h"
#include "../shared/AnimationStructure.h"

bool PackData(const char* inFileDir, const char* outFileName) {
    FILE *dataFile, *outFile;
    if (0 < fopen_s(&outFile, outFileName, "wb") || outFile == nullptr) {
        printf("Error opening %s for output!\n", outFileName);
        return false;
    }

    std::string indexFileName(inFileDir);
    indexFileName.append("index.txt");
    std::ifstream indexFile(indexFileName);
    if (!indexFile.is_open()) {
        printf("Error opening %s for input!\n", indexFileName.c_str());
        return false;
    }
    std::string token;
    std::getline(indexFile, token);

    DataHeader header;
    header.entryCount = std::atoi(token.c_str());
    header.entries = new DataHeaderEntry[header.entryCount];
    
    uint32_t dataOffset = ((header.entryCount * 52 + 16) / 512 + 1) * 512;
    fseek(outFile, dataOffset, SEEK_SET);

    for (uint32_t i = 0; i < header.entryCount; i++) {
        std::getline(indexFile, token, ' ');
        std::strcpy(header.entries[i].name, token.c_str());
        std::getline(indexFile, token);
        header.entries[i].unk = std::atoi(token.c_str());
        std::string dataFileName(inFileDir);
        dataFileName.append(header.entries[i].name);

        if (std::strcmp(header.entries[i].name, "MAPPACK.DAT") == 0)
            dataFileName.append("2");

        if (0 < fopen_s(&dataFile, dataFileName.c_str(), "rb") || dataFile == nullptr) {
            printf("Error opening %s for input!\n", dataFileName.c_str());
            return false;
        }

        header.entries[i].start = ftell(outFile);
        fseek(dataFile, 0, SEEK_END);
        header.entries[i].size = ftell(dataFile);
        fseek(dataFile, 0, SEEK_SET);

        char* content = new char[header.entries[i].size];
        fread(content, 1, header.entries[i].size, dataFile);
        fwrite(content, 1, header.entries[i].size, outFile);
        delete[] content;

        //padding
        size_t size = ftell(outFile);
        if (size % 512 != 0) {
            size = (size / 512 + 1) * 512 - 1;
            fseek(outFile, size, SEEK_SET);
            fputc(0, outFile);
        }

        printf("%s %x %x\n", header.entries[i].name, header.entries[i].start, header.entries[i].size);
    }

    fseek(outFile, 0, SEEK_SET);
    fwrite("PSPFS_V1", 1, 8, outFile);
    fwrite((void*)&header.entryCount, 1, 4, outFile);
    fwrite((void*)&header.empty, 1, 4, outFile);
    for (uint32_t i = 0; i < header.entryCount; i++)
        fwrite((void*)&header.entries[i], 1, 52, outFile);

    indexFile.close();
    fclose(outFile);
    delete[] header.entries;
    return true;
}

bool PackStartData(const char* inFileDir, const char* outFileName, bool isMappack) {
    FILE *dataFile, *outFile;
    if (0 < fopen_s(&outFile, outFileName, "wb") || outFile == nullptr) {
        printf("Error opening %s for output!\n", outFileName);
        return false;
    }

    std::string indexFileName(inFileDir);
    indexFileName.append("index.txt");
    std::ifstream indexFile(indexFileName);
    if (!indexFile.is_open()) {
        printf("Error opening %s for input!\n", indexFileName.c_str());
        return false;
    }
    std::string token;
    std::getline(indexFile, token);

    StartDataHeader header;
    header.entryCount = std::atoi(token.c_str());
    header.entries = new StartDataHeaderEntry[header.entryCount];
    fseek(outFile, header.entryCount * 32 + 16, SEEK_SET);

    for (uint32_t i = 0; i < header.entryCount; i++) {
        std::getline(indexFile, token);
        std::strcpy(header.entries[i].name, token.c_str());
        std::string dataFileName(inFileDir);
        dataFileName.append(token.c_str());

        if (isMappack)
            if (dataFileName.substr(dataFileName.length() - 3) == "chr"/* || dataFileName.substr(dataFileName.length() - 3) == "pos" */)
                dataFileName.append("2");

        if (0 < fopen_s(&dataFile, dataFileName.c_str(), "rb") || dataFile == nullptr) {
            printf("Error opening %s for input!\n", dataFileName.c_str());
            return false;
        }
        size_t filesize = 0;
        fseek(dataFile, 0, SEEK_END);
        filesize = ftell(dataFile);
        fseek(dataFile, 0, SEEK_SET);

        char* content = new char[filesize];
        fread(content, 1, filesize, dataFile);
        fwrite(content, 1, filesize, outFile);
        delete[] content;

        filesize = ftell(outFile);
        header.entries[i].end = filesize - (header.entryCount * 32 + 16);
        printf("%s %x\n", header.entries[i].name, header.entries[i].end);

        fclose(dataFile);
    }
    
    fseek(outFile, 0, SEEK_SET);
    fwrite((void*)&header.entryCount, 1, 4, outFile);
    fwrite((void*)&header.empty, 1, 12, outFile);
    for (uint32_t i = 0; i < header.entryCount; i++)
        fwrite((void*)&header.entries[i], 1, 32, outFile);

    indexFile.close();
    fclose(outFile);
    delete[] header.entries;
    return true;
}

bool PackNispack(const char* inFileDir, const char* outFileName, bool isMappack) {
    FILE* dataFile, * outFile;
    if (0 < fopen_s(&outFile, outFileName, "wb") || outFile == nullptr) {
        printf("Error opening %s for output!\n", outFileName);
        return false;
    }

    std::string indexFileName(inFileDir);
    indexFileName.append("index.txt");
    std::ifstream indexFile(indexFileName);
    if (!indexFile.is_open()) {
        printf("Error opening %s for input!\n", indexFileName.c_str());
        return false;
    }
    std::string token;
    std::getline(indexFile, token);

    NispackHeader header;
    header.entryCount = std::atoi(token.c_str());
    header.entries = new NispackHeaderEntry[header.entryCount];

    uint32_t dataOffset = ((header.entryCount * sizeof(NispackHeaderEntry) + 16) / 2048 + 1) * 2048;
    fseek(outFile, dataOffset, SEEK_SET);

    for (uint32_t i = 0; i < header.entryCount; i++) {
        std::getline(indexFile, token, ' ');
        std::strcpy(header.entries[i].name, token.c_str());
        std::getline(indexFile, token);
        header.entries[i].timestamp = std::atoi(token.c_str());
        std::string inFileName(inFileDir);
        inFileName.append(header.entries[i].name);

        if (inFileName.substr(inFileName.length() - 3) == "lzs") {
            if (isMappack) {
                std::string datFileDir(header.entries[i].name);
                datFileDir.resize(datFileDir.length() - 4);

                
                if (datFileDir == "mp01315" || datFileDir == "mp01316" || datFileDir == "mp01704" || datFileDir == "mp06601" || datFileDir == "mp06701" ||
                    datFileDir == "mp06801" || datFileDir == "mp07001" || datFileDir == "mp12001" || datFileDir == "mp21001" || datFileDir == "mp30004") {
                    //ignore the following maps as those have some strange data
                    //01315 DW Final Decision
                    //01316 DW Viewing Room
                    //01704 The Dark Hero!
                    //06601 For Axel Demo
                    //06701 Axel's Home
                    //06801 Axel's Base
                    //07001 EMPTY 
                    //12001 Dark Court
                    //21001 Local Assembly
                    //30004 Arena Room 
                }
                else {
                    inFileName.append("2");
                    if (!PackStartData((inFileDir + datFileDir + "/").c_str(), (inFileDir + datFileDir + "/" + datFileDir + ".dat2").c_str(), true))
                        return false;
                    if (!CompressData((inFileDir + datFileDir + "/" + datFileDir + ".dat2").c_str(), inFileName.c_str()))
                        return false;
                }
                
            }
            else {
                std::string datFileName(inFileName);
                datFileName.replace(datFileName.end() - 3, datFileName.end(), "dat");
                if (!CompressData(datFileName.c_str(), inFileName.c_str()))
                    return false;
            }
        }

        if (0 < fopen_s(&dataFile, inFileName.c_str(), "rb") || dataFile == nullptr) {
            printf("Error opening %s for input!\n", inFileName.c_str());
            return false;
        }

        header.entries[i].start = ftell(outFile);
        fseek(dataFile, 0, SEEK_END);
        header.entries[i].size = ftell(dataFile);
        fseek(dataFile, 0, SEEK_SET);

        char* content = new char[header.entries[i].size];
        fread(content, 1, header.entries[i].size, dataFile);
        fwrite(content, 1, header.entries[i].size, outFile);
        delete[] content;

        //padding
        if (i < header.entryCount - 1) {
            size_t size = ftell(outFile);
            if (size % 2048 != 0) {
                size = (size / 2048 + 1) * 2048 - 1;
                fseek(outFile, size, SEEK_SET);
                fputc(0, outFile);
            }
        }

        //printf("%s %x %x\n", header.entries[i].name, header.entries[i].start, header.entries[i].size);
    }

    fseek(outFile, 0, SEEK_SET);
    fwrite("NISPACK", 1, 8, outFile);
    fwrite((void*)&header.empty, 1, 4, outFile);
    fwrite((void*)&header.entryCount, 1, 4, outFile);
    fwrite((void*)header.entries, sizeof(NispackHeaderEntry), header.entryCount, outFile);

    indexFile.close();
    fclose(outFile);
    delete[] header.entries;
    return true;
}

bool PackAnimation() {
    return !useAnimation || PackNispack("../extracted/sub_data/anmpack/", "../extracted/sub_data/ANMPACK.DAT", false);
}

bool PackMaps() {
    return PackNispack("../extracted/data/mappack/", "../extracted/data/MAPPACK.DAT2", true);
}

int main()
{
    if (!ReadTxtFiles())
        return 1;
    if (!PackStartData("../extracted/sub_data/start/", "../extracted/sub_data/start.dat", false))
        return 1;
    if (!PackStartData("../extracted/sub_data/start_vm/", "../extracted/sub_data/start_vm.dat", false))
        return 1;
    if (!CompressData("../extracted/sub_data/start_vm.dat", "../extracted/sub_data/START_VM.LZS"))
        return 1;
    if (!CompressData("../extracted/sub_data/start.dat", "../extracted/sub_data/START.LZS"))
        return 1;
    if (!PackAnimation())
        return 1;
    if (!PackData("../extracted/sub_data/", "../../SUB_D001.DAT"))
        return 1;

    if (!ReadMapFiles())
        return 1;
    if (!PackMaps())
        return 1;
    if (!PackData("../extracted/data/", "../../D001.DAT"))
        return 1;
    printf("\nDone!");
    return 0;
}