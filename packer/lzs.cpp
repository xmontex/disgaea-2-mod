#include<stdio.h>
#include<stdlib.h>
#include <iostream>


#define CONTROL_LITERAL 0
#define CONTROL_CODEWORD 1

#define MAX_LENGTH 254
#define ITERATIONS_BEFORE_CALLBACK 256

struct LzsHeader {
    char extension[4];
    uint32_t decompressedSize;
    uint32_t compressedSize;
    unsigned char magicChar;
    char padding[3];
};

unsigned char DataCompare(unsigned char *str1, unsigned char *str2, unsigned char maxlength) {
    unsigned char length = 0;
    for (; *str1 == *str2 && length < maxlength; length++, str1++, str2++);
    return length;
}

unsigned char *SearchForPhrase(unsigned char *src, unsigned char *windowstart, unsigned char maxlength, unsigned char *bestlength) {
    unsigned char *srcnow = src;
    unsigned char *best = NULL;
    unsigned char curlength = 0;
    *bestlength = 1;
    srcnow -= 4; //this is where we can start gaining some size decrease already
    while (srcnow >= windowstart) {
        unsigned char length = maxlength;
        if (length > src - srcnow)
            length = src - srcnow;
        curlength = DataCompare(srcnow, src, length);
        if (*bestlength < curlength) {
            *bestlength = curlength;
            best = srcnow;
        }
        srcnow--;
    }
    return best;
}

unsigned char FindBestMagicChar(unsigned char *src, fpos_t filesize) {
    uint32_t counts[256];
    for (int i = 0; i < 256; i++)
        counts[i] = 0;

    for (int i = 0; i < filesize; i++)
        counts[src[i]]++;

    uint32_t leastCount = 0xffffffff;
    unsigned char leastChar = 0;
    for (int i = 0; i < 256; i++)
        if (counts[i] < leastCount) {
            leastChar = i;
            leastCount = counts[i];
        }
    return leastChar;
}

bool CompressData(const char* inFileName, const char* outFileName) {
    FILE *inFile, *outFile;
    unsigned int newsize = 0;
    fpos_t filesize = 0;
    unsigned char *src, *dest;
    if (0 < fopen_s(&inFile, inFileName, "rb")) {
        printf("Error opening %s for input!\n", inFileName);
        return false;
    }
    if (0 < fopen_s(&outFile, outFileName, "wb")) {
        printf("Error opening %s for output!\n", outFileName);
        return false;
    }
    fseek(inFile, 0, SEEK_END);
    filesize = ftell(inFile);
    fseek(inFile, 0, SEEK_SET);
    src = (unsigned char*)malloc((size_t)filesize);
    dest = (unsigned char*)malloc((size_t)filesize);
    fread(src, 1, (size_t)filesize, inFile);

    const unsigned char magicChar = FindBestMagicChar(src, filesize);
  
    unsigned char *src_end = src + filesize;
    unsigned char *dest_end = dest + filesize;
    unsigned char *dest_start = dest;
    unsigned char *srcnow = src;
    unsigned long iterationcnt = 0;
    unsigned char phrase_length;
    unsigned char maxlength;
    while (srcnow < src_end && dest < dest_end) {
        maxlength = MAX_LENGTH;
        if (maxlength > src_end - src) //dont go beyond star of the file
                maxlength = (unsigned char)(src_end - src);
        unsigned char *windowstart = srcnow - MAX_LENGTH;
        if (windowstart < src)
            windowstart = src;
        unsigned char *phrase_ptr = SearchForPhrase(srcnow, windowstart, maxlength, &phrase_length);
        if (phrase_length <= 3) { //not worth replacing as we don't save space
            if (*srcnow == magicChar) //if data is same as magicChar then write it twice
                *dest++ = *srcnow;
            *dest++ = *srcnow++;
        }
        else {
            *dest++ = magicChar;
            unsigned char offset = srcnow - phrase_ptr;
            if (offset >= magicChar) //if data is same as magicChar then write it twice; thus offset has to shift by one
                offset++;
            *dest++ = offset;
            *dest++ = phrase_length;
            srcnow += phrase_length;
        }
        iterationcnt++;
        if (iterationcnt == ITERATIONS_BEFORE_CALLBACK) {
            iterationcnt = 0;
            printf("\r%d%% complete.   ", (srcnow - src) * 100 / (unsigned int)filesize);
        }

    }
    printf("\r%d%% complete.   ", (srcnow - src) * 100 / (unsigned int)filesize);

    newsize = (unsigned int)(dest - dest_start + 12);
    fwrite("dat\0", 1, 4, outFile);
    fwrite((void *)&filesize, 1, 4, outFile);
    fwrite((void *)&newsize, 1, 4, outFile);
    fwrite((void *)&magicChar, 1, 1, outFile);
    fwrite("\0\0\0", 1, 3, outFile); //padding
    fwrite(dest_start, 1, (size_t)newsize - 12, outFile);
    printf("\nOriginal size: %u  Compressed Size: %u.\n", (unsigned int)filesize, newsize + 16);

    free(src);
    free(dest_start);
    fclose(inFile);
    fclose(outFile);
    return true;
}