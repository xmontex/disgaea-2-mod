#include <inttypes.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <stdio.h>
#include "StartData.h"
#include "../shared/StartDataStructure.h"

bool ReadTxtChar() {
    std::ifstream inFile("../extracted/txt/char.txt");
    if (!inFile.is_open()) {
        printf("Error opening ../extracted/txt/char.txt for input!\n");
        return false;
    }

    FILE *outFile;
    if (0 < fopen_s(&outFile, "../extracted/sub_data/start/char.dat", "wb") || outFile == nullptr) {
        printf("Error opening ../extracted/sub_data/start/char.dat for output!\n");
        return false;
    }

    
    std::string line;
    std::getline(inFile, line);
    std::getline(inFile, line);

    character data;
    int recordCount = 0;
    fwrite((void*)&recordCount, 4, 1, outFile);
    while (std::getline(inFile, line)) {
        std::stringstream stream(line);
        std::string token;
        std::getline(stream, token, '\t');
        std::strcpy(data.name, token.c_str());
        std::getline(stream, token, '\t');
        std::strcpy(data.title, token.c_str());
        std::getline(stream, token, '\t');
        data.gender = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.spellCount = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.tier = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk1 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.jumpHeight = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.profFists = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.profSword = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.profSpear = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.profBow = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.profGun = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.profAxe = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.profStaff = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk2 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.specAbility = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.aptHP = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.aptSP = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.aptATK = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.aptDEF = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.aptINT = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.aptSPD = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.aptHIT = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.aptRES = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk3 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.type = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.throwDist = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.counter = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk4 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk5 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk6 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.id = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.first = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk7 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk8 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.desc = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.statHP = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.statSP = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.statATK = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.statDEF = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.statINT = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.statSPD = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.statHIT = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.statRES = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.expRate = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.moveDist = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.canFly = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.attackRange = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.killHell = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.killExp = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk9 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.createMana = std::atoi(token.c_str());
        for (int i = 0; i < 32; i++) {
            std::getline(stream, token, '\t');
            data.spellId[i] = std::atoi(token.c_str());
        }
        for (int i = 0; i < 32; i++) {
            std::getline(stream, token, '\t');
            data.spellLearnedAt[i] = std::atoi(token.c_str());
        }
        std::getline(stream, token, '\t');
        data.id2 = std::atoi(token.c_str());
        for (int i = 0; i < 15; i++) {
            std::getline(stream, token, ' ');
            data.empty[i] = std::atoi(token.c_str());
        }

        recordCount++;
        fwrite((void*)&data, sizeof(data), 1, outFile);
        data = character(); //reset is required
    }

    //padding
    size_t size = ftell(outFile);
    if (size % 16 != 0) {
        size = (size / 16 + 1) * 16 - 1;
        fseek(outFile, size, SEEK_SET);
        fputc(0, outFile);
    }

    //proper record count
    fseek(outFile, 0, SEEK_SET);
    fwrite((void*)&recordCount, 4, 1, outFile);

    inFile.close();
    fclose(outFile);
    return true;
}

bool ReadTxtCharHelp() {
    std::ifstream inFile("../extracted/txt/charhelp.txt");
    if (!inFile.is_open()) {
        printf("Error opening ../extracted/txt/charhelp.txt for input!\n");
        return false;
    }

    FILE *outFile;
    if (0 < fopen_s(&outFile, "../extracted/sub_data/start/charhelp.dat", "wb") || outFile == nullptr) {
        printf("Error opening ../extracted/sub_data/start/charhelp.dat for output!\n");
        return false;
    }

    std::string line;
    std::getline(inFile, line);
    std::getline(inFile, line);

    characterhelp data;
    int recordCount = 0;
    fwrite((void*)&recordCount, 4, 1, outFile);
    while (std::getline(inFile, line)) {
        std::stringstream stream(line);
        std::string token;
        std::getline(stream, token, '\t');
        std::strcpy(data.line1, token.c_str());
        std::getline(stream, token, '\t');
        std::strcpy(data.line2, token.c_str());
        std::getline(stream, token, '\t');
        std::strcpy(data.line3, token.c_str());
        std::getline(stream, token, '\t');
        data.id = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.id2 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.empty = std::atoi(token.c_str());
        recordCount++;
        fwrite((void*)&data, sizeof(data), 1, outFile);
        data = characterhelp(); //reset is required
    }

    //padding
    size_t size = ftell(outFile);
    if (size % 16 != 0) {
        size = (size / 16 + 1) * 16 - 1;
        fseek(outFile, size, SEEK_SET);
        fputc(0, outFile);
    }

    //proper record count
    fseek(outFile, 0, SEEK_SET);
    fwrite((void*)&recordCount, 4, 1, outFile);

    inFile.close();
    fclose(outFile);
    return true;
}

bool ReadTxtDungeon() {
    std::ifstream inFile("../extracted/txt/dungeon.txt");
    if (!inFile.is_open()) {
        printf("Error opening ../extracted/txt/dungeon.txt for input!\n");
        return false;
    }

    FILE *outFile;
    if (0 < fopen_s(&outFile, "../extracted/sub_data/start/dungeon.dat", "wb") || outFile == nullptr) {
        printf("Error opening ../extracted/sub_data/start/dungeon.dat for output!\n");
        return false;
    }

    std::string line;
    std::getline(inFile, line);
    std::getline(inFile, line);

    dungeon data;
    int recordCount = 0;
    fwrite((void*)&recordCount, 4, 1, outFile);
    while (std::getline(inFile, line)) {
        std::stringstream stream(line);
        std::string token;
        std::getline(stream, token, '\t');
        std::strcpy(data.name, token.c_str());
        std::getline(stream, token, '\t');
        data.id = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.dwTarget = std::atoi(token.c_str());
        for (int i = 0; i < 3; i++) {
            std::getline(stream, token, ' ');
            data.empty[i] = std::atoi(token.c_str());
        }
        std::getline(stream, token, '\t');
        std::getline(stream, token, '\t');
        data.unk1 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.bonusRank = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.dwCondition = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.empty2 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.dwUnlock = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.removeUnique1 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.empty3 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.dwFlag = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.removeUnique2 = std::atoi(token.c_str());
        for (int i = 0; i < 23; i++) {
            std::getline(stream, token, ' ');
            data.empty4[i] = std::atoi(token.c_str());
        }
        std::getline(stream, token, '\t');
        std::getline(stream, token, '\t');
        data.dwMap = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.dwLevel = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.id2 = std::atoi(token.c_str());
        for (int i = 0; i < 3; i++) {
            std::getline(stream, token, ' ');
            data.empty5[i] = std::atoi(token.c_str());
        }

        recordCount++;
        fwrite((void*)&data, sizeof(data), 1, outFile);
        data = dungeon(); //reset is required
    }

    //padding
    size_t size = ftell(outFile);
    if (size % 16 != 0) {
        size = (size / 16 + 1) * 16 - 1;
        fseek(outFile, size, SEEK_SET);
        fputc(0, outFile);
    }

    //proper record count
    fseek(outFile, 0, SEEK_SET);
    fwrite((void*)&recordCount, 4, 1, outFile);

    inFile.close();
    fclose(outFile);
    return true;
}

bool ReadTxtHabit() {
    std::ifstream inFile("../extracted/txt/HABIT.txt");
    if (!inFile.is_open()) {
        printf("Error opening ../extracted/txt/HABIT.txt for input!\n");
        return false;
    }

    FILE *outFile;
    if (0 < fopen_s(&outFile, "../extracted/sub_data/start/HABIT.dat", "wb") || outFile == nullptr) {
        printf("Error opening ../extracted/sub_data/start/HABIT.dat for output!\n");
        return false;
    }

    std::string line;
    std::getline(inFile, line);
    std::getline(inFile, line);

    habit data;
    int recordCount = 0;
    fwrite((void*)&recordCount, 4, 1, outFile);
    while (std::getline(inFile, line)) {
        std::stringstream stream(line);
        std::string token;
        std::getline(stream, token, '\t');
        std::strcpy(data.name, token.c_str());
        std::getline(stream, token, '\t');
        data.chanceRecovery = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.chanceFist = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.chanceSword = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.chanceSpear = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.chanceBow = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.chanceGun = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.chanceAxe = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.chanceStaff = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.chanceMeleeClaw = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.chanceCasterClaw = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.chanceArmor = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.chanceBelt = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.chanceShoes = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.chanceOrb = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.chanceGlasses = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.chanceMuscle = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.chanceWeight = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.chanceMap = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.chanceEmblem = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.chanceLegendary = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk21 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk22 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.id = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.maxStack = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.id2 = std::atoi(token.c_str());
        for (int i = 0; i < 15; i++) {
            std::getline(stream, token, ' ');
            data.empty[i] = std::atoi(token.c_str());
        }

        recordCount++;
        fwrite((void*)&data, sizeof(data), 1, outFile);
        data = habit(); //reset is required
    }

    //padding
    size_t size = ftell(outFile);
    if (size % 16 != 0) {
        size = (size / 16 + 1) * 16 - 1;
        fseek(outFile, size, SEEK_SET);
        fputc(0, outFile);
    }

    //proper record count
    fseek(outFile, 0, SEEK_SET);
    fwrite((void*)&recordCount, 4, 1, outFile);

    inFile.close();
    fclose(outFile);
    return true;
}

bool ReadTxtMagic() {
    std::ifstream inFile("../extracted/txt/magic.txt");
    if (!inFile.is_open()) {
        printf("Error opening ../extracted/txt/magic.txt for input!\n");
        return false;
    }

    FILE *outFile;
    if (0 < fopen_s(&outFile, "../extracted/sub_data/start/magic.dat", "wb") || outFile == nullptr) {
        printf("Error opening ../extracted/sub_data/start/magic.dat for output!\n");
        return false;
    }

    std::string line;
    std::getline(inFile, line);
    std::getline(inFile, line);

    magic data;
    int recordCount = 0;
    fwrite((void*)&recordCount, 4, 1, outFile);
    while (std::getline(inFile, line)) {
        std::stringstream stream(line);
        std::string token;
        std::getline(stream, token, '\t');
        data.id = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk1 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.weaponLvl = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk2 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.cost = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        std::strcpy(data.name, token.c_str());
        std::getline(stream, token, '\t');
        std::strcpy(data.desc, token.c_str());
        for (int i = 0; i < 6; i++) {
            std::getline(stream, token, '\t');
            data.effectValue[i] = std::atoi(token.c_str());
        }
        std::getline(stream, token, '\t');
        data.weaponType = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.element = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.spellType = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.weaponType2 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk3 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.targetRange = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.targetInLine = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.targetShape = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.minHeight = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.maxHeight = std::atoi(token.c_str());
        for (int i = 0; i < 6; i++) {
            std::getline(stream, token, '\t');
            data.effectType[i] = std::atoi(token.c_str());
        }
        std::getline(stream, token, '\t');
        data.id2 = std::atoi(token.c_str());
        for (int i = 0; i < 11; i++) {
            std::getline(stream, token, ' ');
            data.empty[i] = std::atoi(token.c_str());
        }

        recordCount++;
        fwrite((void*)&data, sizeof(data), 1, outFile);
        data = magic(); //reset is required
    }

    //padding
    size_t size = ftell(outFile);
    if (size % 16 != 0) {
        size = (size / 16 + 1) * 16 - 1;
        fseek(outFile, size, SEEK_SET);
        fputc(0, outFile);
    }

    //proper record count
    fseek(outFile, 0, SEEK_SET);
    fwrite((void*)&recordCount, 4, 1, outFile);

    inFile.close();
    fclose(outFile);
    return true;
}

bool ReadTxtMitem() {
    std::ifstream inFile("../extracted/txt/mitem.txt");
    if (!inFile.is_open()) {
        printf("Error opening ../extracted/txt/mitem.txt for input!\n");
        return false;
    }

    FILE *outFile;
    if (0 < fopen_s(&outFile, "../extracted/sub_data/start/mitem.dat", "wb") || outFile == nullptr) {
        printf("Error opening ../extracted/sub_data/start/mitem.dat for output!\n");
        return false;
    }

    std::string line;
    std::getline(inFile, line);
    std::getline(inFile, line);

    mitem data;
    int recordCount = 0;
    fwrite((void*)&recordCount, 4, 1, outFile);
    while (std::getline(inFile, line)) {
        std::stringstream stream(line);
        std::string token;
        std::getline(stream, token, '\t');
        std::strcpy(data.name, token.c_str());
        std::getline(stream, token, '\t');
        std::strcpy(data.desc, token.c_str());
        std::getline(stream, token, '\t');
        data.statid = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.statvalue = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.rank = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.range = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.jump = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk4 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.critChance = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.scrollSkill = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.type = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.statHP = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.statSP = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.statATK = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.statDEF = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.statINT = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.statSPD = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.statHIT = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.statRES = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.id = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.icon = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.type2 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.move = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.specialist = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.sprite = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.price = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.id2 = std::atoi(token.c_str());
        for (int i = 0; i < 3; i++) {
            std::getline(stream, token, ' ');
            data.empty[i] = std::atoi(token.c_str());
        }

        recordCount++;
        fwrite((void*)&data, sizeof(data), 1, outFile);
        data = mitem(); //reset is required
    }

    //padding
    size_t size = ftell(outFile);
    if (size % 16 != 0) {
        size = (size / 16 + 1) * 16 - 1;
        fseek(outFile, size, SEEK_SET);
        fputc(0, outFile);
    }

    //proper record count
    fseek(outFile, 0, SEEK_SET);
    fwrite((void*)&recordCount, 4, 1, outFile);

    inFile.close();
    fclose(outFile);
    return true;
}

bool ReadTxtDarkSun1() {
    std::ifstream inFile("../extracted/txt/darksun1.txt");
    if (!inFile.is_open()) {
        printf("Error opening ../extracted/txt/darksun1.txt for input!\n");
        return false;
    }

    FILE *outFile;
    if (0 < fopen_s(&outFile, "../extracted/sub_data/start_vm/darksun1.dat", "wb") || outFile == nullptr) {
        printf("Error opening ../extracted/sub_data/start_vm/darksun1.dat for output!\n");
        return false;
    }

    std::string line;
    std::getline(inFile, line);
    std::getline(inFile, line);

    darksun1 data;
    int recordCount = 0;
    fwrite((void*)&recordCount, 4, 1, outFile);
    while (std::getline(inFile, line)) {
        std::stringstream stream(line);
        std::string token;
        std::getline(stream, token, '\t');
        std::strcpy(data.name, token.c_str());
        std::getline(stream, token, '\t');
        data.unk1 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk2 = std::atoi(token.c_str());
        for (int i = 0; i < 8; i++) {
            std::getline(stream, token, '\t');
            data.group[i] = std::atoi(token.c_str());
        }
        std::getline(stream, token, '\t');
        data.id = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk3 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.target = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.id2 = std::atoi(token.c_str());
        for (int i = 0; i < 17; i++) {
            std::getline(stream, token, ' ');
            data.empty[i] = std::atoi(token.c_str());
        }

        recordCount++;
        fwrite((void*)&data, sizeof(data), 1, outFile);
        data = darksun1(); //reset is required
    }

    //padding
    size_t size = ftell(outFile);
    if (size % 16 != 0) {
        size = (size / 16 + 1) * 16 - 1;
        fseek(outFile, size, SEEK_SET);
        fputc(0, outFile);
    }

    //proper record count
    fseek(outFile, 0, SEEK_SET);
    fwrite((void*)&recordCount, 4, 1, outFile);

    inFile.close();
    fclose(outFile);
    return true;
}

bool ReadTxtDarkSun2() {
    std::ifstream inFile("../extracted/txt/darksun2.txt");
    if (!inFile.is_open()) {
        printf("Error opening ../extracted/txt/darksun2.txt for input!\n");
        return false;
    }

    FILE *outFile;
    if (0 < fopen_s(&outFile, "../extracted/sub_data/start_vm/darksun2.dat", "wb") || outFile == nullptr) {
        printf("Error opening ../extracted/sub_data/start_vm/darksun2.dat for output!\n");
        return false;
    }

    std::string line;
    std::getline(inFile, line);
    std::getline(inFile, line);

    darksun2 data;
    int recordCount = 0;
    fwrite((void*)&recordCount, 4, 1, outFile);
    while (std::getline(inFile, line)) {
        std::stringstream stream(line);
        std::string token;
        std::getline(stream, token, '\t');
        std::strcpy(data.name, token.c_str());
        std::getline(stream, token, '\t');
        data.unk1 = std::atoi(token.c_str());
        for (int i = 0; i < 8; i++) {
            std::getline(stream, token, '\t');
            data.group[i] = std::atoi(token.c_str());
        }
        std::getline(stream, token, '\t');
        data.unk2 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.id = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk3 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.target = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.id2 = std::atoi(token.c_str());
        for (int i = 0; i < 13; i++) {
            std::getline(stream, token, ' ');
            data.empty[i] = std::atoi(token.c_str());
        }

        recordCount++;
        fwrite((void*)&data, sizeof(data), 1, outFile);
        data = darksun2(); //reset is required
    }

    //padding
    size_t size = ftell(outFile);
    if (size % 16 != 0) {
        size = (size / 16 + 1) * 16 - 1;
        fseek(outFile, size, SEEK_SET);
        fputc(0, outFile);
    }

    //proper record count
    fseek(outFile, 0, SEEK_SET);
    fwrite((void*)&recordCount, 4, 1, outFile);

    inFile.close();
    fclose(outFile);
    return true;
}

bool ReadTxtDkTable() {
    std::ifstream inFile("../extracted/txt/dktable.txt");
    if (!inFile.is_open()) {
        printf("Error opening ../extracted/txt/dktable.txt for input!\n");
        return false;
    }

    FILE *outFile;
    if (0 < fopen_s(&outFile, "../extracted/sub_data/start_vm/dktable.dat", "wb") || outFile == nullptr) {
        printf("Error opening ../extracted/sub_data/start_vm/dktable.dat for output!\n");
        return false;
    }

    std::string line;
    std::getline(inFile, line);
    std::getline(inFile, line);

    dktable data;
    int recordCount = 0;
    fwrite((void*)&recordCount, 4, 1, outFile);
    fwrite((void*)&recordCount, 4, 1, outFile);
    while (std::getline(inFile, line)) {
        std::stringstream stream(line);
        std::string token;
        std::getline(stream, token, '\t');
        data.mapId = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.count = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.difficulty = std::atoi(token.c_str());
        for (int i = 0; i < 32; i++) {
            std::getline(stream, token, '\t');
            data.dkGroup[i] = std::atoi(token.c_str());
        }
        for (int i = 0; i < 32; i++) {
            std::getline(stream, token, '\t');
            data.dkSun[i] = std::atoi(token.c_str());
        }
        for (int i = 0; i < 32; i++) {
            std::getline(stream, token, '\t');
            data.dkEffect[i] = std::atoi(token.c_str());
        }
        for (int i = 0; i < 32; i++) {
            std::getline(stream, token, '\t');
            data.dkEmpty[i] = std::atoi(token.c_str());
        }

        recordCount++;
        fwrite((void*)&data, sizeof(data), 1, outFile);
        data = dktable(); //reset is required
    }

    //padding
    size_t size = ftell(outFile);
    if (size % 16 != 0) {
        size = (size / 16 + 1) * 16 - 1;
        fseek(outFile, size, SEEK_SET);
        fputc(0, outFile);
    }

    //proper record count
    fseek(outFile, 0, SEEK_SET);
    fwrite((void*)&recordCount, 4, 1, outFile);
    fwrite((void*)&recordCount, 4, 1, outFile);

    inFile.close();
    fclose(outFile);
    return true;
}

bool ReadTxtHospital() {
    std::ifstream inFile("../extracted/txt/HOSPITAL.txt");
    if (!inFile.is_open()) {
        printf("Error opening ../extracted/txt/HOSPITAL.txt for input!\n");
        return false;
    }

    FILE *outFile;
    if (0 < fopen_s(&outFile, "../extracted/sub_data/start_vm/HOSPITAL.dat", "wb") || outFile == nullptr) {
        printf("Error opening ../extracted/sub_data/start_vm/HOSPITAL.dat for output!\n");
        return false;
    }

    std::string line;
    std::getline(inFile, line);
    std::getline(inFile, line);

    hospital data;
    int recordCount = 0;
    fwrite((void*)&recordCount, 4, 1, outFile);
    fwrite((void*)&recordCount, 4, 1, outFile);
    while (std::getline(inFile, line)) {
        std::stringstream stream(line);
        std::string token;
        std::getline(stream, token, '\t');
        data.id = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.revive = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.reward = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.restoreHP = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.restoreSP = std::atoi(token.c_str());

        recordCount++;
        fwrite((void*)&data, sizeof(data), 1, outFile);
        data = hospital(); //reset is required
    }

    //padding
    size_t size = ftell(outFile);
    if (size % 16 != 0) {
        size = (size / 16 + 1) * 16 - 1;
        fseek(outFile, size, SEEK_SET);
        fputc(0, outFile);
    }

    //proper record count
    fseek(outFile, 0, SEEK_SET);
    fwrite((void*)&recordCount, 4, 1, outFile);
    fwrite((void*)&recordCount, 4, 1, outFile);

    inFile.close();
    fclose(outFile);
    return true;
}

bool ReadTxtJudgment() {
    std::ifstream inFile("../extracted/txt/judgment.txt");
    if (!inFile.is_open()) {
        printf("Error opening ../extracted/txt/judgment.txt for input!\n");
        return false;
    }

    FILE *outFile;
    if (0 < fopen_s(&outFile, "../extracted/sub_data/start_vm/judgment.dat", "wb") || outFile == nullptr) {
        printf("Error opening ../extracted/sub_data/start_vm/judgment.dat for output!\n");
        return false;
    }

    std::string line;
    std::getline(inFile, line);
    std::getline(inFile, line);

    judgment data;
    int recordCount = 0;
    fwrite((void*)&recordCount, 4, 1, outFile);
    while (std::getline(inFile, line)) {
        std::stringstream stream(line);
        std::string token;
        std::getline(stream, token, '\t');
        std::strcpy(data.name, token.c_str());
        std::getline(stream, token, '\t');
        data.id = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.worldLevel = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.felonyCount = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.goalType = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.rewardType = std::atoi(token.c_str());
        for (int i = 0; i < 19; i++) {
            std::getline(stream, token, ' ');
            data.empty[i] = std::atoi(token.c_str());
        }
        std::getline(stream, token, '\t');
        std::getline(stream, token, '\t');
        data.goal = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.reward = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.id2 = std::atoi(token.c_str());
        for (int i = 0; i < 27; i++) {
            std::getline(stream, token, ' ');
            data.empty2[i] = std::atoi(token.c_str());
        }

        recordCount++;
        fwrite((void*)&data, sizeof(data), 1, outFile);
        data = judgment(); //reset is required
    }

    //padding
    size_t size = ftell(outFile);
    if (size % 16 != 0) {
        size = (size / 16 + 1) * 16 - 1;
        fseek(outFile, size, SEEK_SET);
        fputc(0, outFile);
    }

    //proper record count
    fseek(outFile, 0, SEEK_SET);
    fwrite((void*)&recordCount, 4, 1, outFile);

    inFile.close();
    fclose(outFile);
    return true;
}

bool ReadTxtPirate() {
    std::ifstream inFile("../extracted/txt/pirate.txt");
    if (!inFile.is_open()) {
        printf("Error opening ../extracted/txt/pirate.txt for input!\n");
        return false;
    }

    FILE *outFile;
    if (0 < fopen_s(&outFile, "../extracted/sub_data/start_vm/pirate.dat", "wb") || outFile == nullptr) {
        printf("Error opening ../extracted/sub_data/start_vm/pirate.dat for output!\n");
        return false;
    }

    std::string line;
    std::getline(inFile, line);
    std::getline(inFile, line);

    pirate data;
    int recordCount = 0;
    fwrite((void*)&recordCount, 4, 1, outFile);
    while (std::getline(inFile, line)) {
        std::stringstream stream(line);
        std::string token;
        std::getline(stream, token, '\t');
        std::strcpy(data.name, token.c_str());
        std::getline(stream, token, '\t');
        data.id = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.worldLevel = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.specialists = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.treasures = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk1 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.crewSize = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.shipType = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.chanceRecovery = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.chanceFist = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.chanceSword = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.chanceSpear = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.chanceBow = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.chanceGun = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.chanceAxe = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.chanceStaff = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.chanceMeleeClaw = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.chanceCasterClaw = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.chanceArmor = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.chanceBelt = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.chanceShoes = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.chanceOrb = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.chanceGlasses = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.chanceMuscle = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.chanceWeight = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.chanceMap = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.chanceEmblem = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.chanceLegendary = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk2 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk3 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk4 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk5 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.pirateId = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.pirateLvlMult = std::atoi(token.c_str());
        for (int i = 0; i < 16; i++) {
            std::getline(stream, token, '\t');
            data.crewId[i] = std::atoi(token.c_str());
        }
        for (int i = 0; i < 16; i++) {
            std::getline(stream, token, '\t');
            data.crewLvlMult[i] = std::atoi(token.c_str());
        }
        std::getline(stream, token, '\t');
        data.id2 = std::atoi(token.c_str());
        for (int i = 0; i < 7; i++) {
            std::getline(stream, token, ' ');
            data.empty[i] = std::atoi(token.c_str());
        }

        recordCount++;
        fwrite((void*)&data, sizeof(data), 1, outFile);
        data = pirate(); //reset is required
    }

    //padding
    size_t size = ftell(outFile);
    if (size % 16 != 0) {
        size = (size / 16 + 1) * 16 - 1;
        fseek(outFile, size, SEEK_SET);
        fputc(0, outFile);
    }

    //proper record count
    fseek(outFile, 0, SEEK_SET);
    fwrite((void*)&recordCount, 4, 1, outFile);

    inFile.close();
    fclose(outFile);
    return true;
}

bool ReadTxtWish() {
    std::ifstream inFile("../extracted/txt/WISH.txt");
    if (!inFile.is_open()) {
        printf("Error opening ../extracted/txt/WISH.txt for input!\n");
        return false;
    }

    FILE *outFile;
    if (0 < fopen_s(&outFile, "../extracted/sub_data/start_vm/WISH.dat", "wb") || outFile == nullptr) {
        printf("Error opening ../extracted/sub_data/start_vm/WISH.dat for output!\n");
        return false;
    }

    std::string line;
    std::getline(inFile, line);
    std::getline(inFile, line);

    wish data;
    int recordCount = 0;
    fwrite((void*)&recordCount, 4, 1, outFile);
    while (std::getline(inFile, line)) {
        std::stringstream stream(line);
        std::string token;
        std::getline(stream, token, '\t');
        data.cost = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk1 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.id = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.senators = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.legendary = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.approval = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        std::strcpy(data.name, token.c_str());
        std::getline(stream, token, '\t');
        std::strcpy(data.desc, token.c_str());
        std::getline(stream, token, '\t');
        data.id2 = std::atoi(token.c_str());
        for (int i = 0; i < 25; i++) {
            std::getline(stream, token, ' ');
            data.empty[i] = std::atoi(token.c_str());
        }

        recordCount++;
        fwrite((void*)&data, sizeof(data), 1, outFile);
        data = wish(); //reset is required
    }

    //padding
    size_t size = ftell(outFile);
    if (size % 16 != 0) {
        size = (size / 16 + 1) * 16 - 1;
        fseek(outFile, size, SEEK_SET);
        fputc(0, outFile);
    }

    //proper record count
    fseek(outFile, 0, SEEK_SET);
    fwrite((void*)&recordCount, 4, 1, outFile);

    inFile.close();
    fclose(outFile);
    return true;
}

bool ReadTxtWish2() {
    std::ifstream inFile("../extracted/txt/WISH2.txt");
    if (!inFile.is_open()) {
        printf("Error opening ../extracted/txt/WISH2.txt for input!\n");
        return false;
    }

    FILE *outFile;
    if (0 < fopen_s(&outFile, "../extracted/sub_data/start/WISH2.dat", "wb") || outFile == nullptr) {
        printf("Error opening ../extracted/sub_data/start/WISH2.dat for output!\n");
        return false;
    }

    std::string line;
    std::getline(inFile, line);
    std::getline(inFile, line);

    wish2 data;
    int recordCount = 0;
    fwrite((void*)&recordCount, 4, 1, outFile);
    while (std::getline(inFile, line)) {
        std::stringstream stream(line);
        std::string token;
        std::getline(stream, token, '\t');
        data.unk1 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk2 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk3 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.proposerId = std::atoi(token.c_str());
        for (int i = 0; i < 8; i++) {
            std::getline(stream, token, ' ');
            data.empty[i] = std::atoi(token.c_str());
        }
        std::getline(stream, token, '\t');
        std::getline(stream, token, '\t');
        data.unk4 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.id = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.group = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        data.unk5 = std::atoi(token.c_str());
        for (int i = 0; i < 2; i++) {
            std::getline(stream, token, ' ');
            data.empty2[i] = std::atoi(token.c_str());
        }
        std::getline(stream, token, '\t');
        std::getline(stream, token, '\t');
        data.unk6 = std::atoi(token.c_str());
        std::getline(stream, token, '\t');
        std::strcpy(data.name, token.c_str());
        std::getline(stream, token, '\t');
        std::strcpy(data.desc, token.c_str());
        std::getline(stream, token, '\t');
        data.id2 = std::atoi(token.c_str());
        for (int i = 0; i < 51; i++) {
            std::getline(stream, token, ' ');
            data.empty3[i] = std::atoi(token.c_str());
        }

        recordCount++;
        fwrite((void*)&data, sizeof(data), 1, outFile);
        data = wish2(); //reset is required
    }

    //padding
    size_t size = ftell(outFile);
    if (size % 16 != 0) {
        size = (size / 16 + 1) * 16 - 1;
        fseek(outFile, size, SEEK_SET);
        fputc(0, outFile);
    }

    //proper record count
    fseek(outFile, 0, SEEK_SET);
    fwrite((void*)&recordCount, 4, 1, outFile);

    inFile.close();
    fclose(outFile);
    return true;
}

bool ReadTxtFiles() {
    if (!ReadTxtChar())
        return false;
    if (!ReadTxtCharHelp())
        return false;
    if (!ReadTxtDungeon())
        return false;
    if (!ReadTxtHabit())
        return false;
    if (!ReadTxtMagic())
        return false;
    if (!ReadTxtMitem())
        return false;
    if (!ReadTxtDarkSun1())
        return false;
    if (!ReadTxtDarkSun2())
        return false;
    if (!ReadTxtDkTable())
        return false;
    if (!ReadTxtHospital())
        return false;
    if (!ReadTxtJudgment())
        return false;
    if (!ReadTxtPirate())
        return false;
    if (!ReadTxtWish())
        return false;
    if (!ReadTxtWish2())
        return false;
    return true;
}