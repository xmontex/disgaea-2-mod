2	2	2	2	1	1	68	126	2	25x2
cost	unk1	id	senators	legendary	approval	name	desc	id2	empty
0	0	50	0	0	0	Senator　List	Displays　the　list　of　senators.	50	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
0	0	51	3	0	0	Create　New　Character	Creates　a　new　character　(pupil).	51	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
0	0	52	0	0	0	Delete　a　Character	Deletes　a　character　permanently.	52	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
0	0	53	0	0	0	Change　Name	Changes　character's　name.	53	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
200	0	56	3	0	0	Reincarnation	Reincarnates　character　(start　from　LV　1).	56	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
1000	0	69	3	0	0	Reincarnate　to　Atone　for　Sins	Reincarnate,　and　confess　all　your　crimes.	69	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
200	10	70	3	0	0	Reincarnate　into　a　Prinny	Reincarnate　into　a　Prinny　to　repent　for　your　crimes.	70	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
1000	10	71	4	0	0	End　Prinny　Reincarnation	Finish　repenting　as　a　Prinny,　and　become　reborn.	71	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
10	3	1	2	0	30	More　Expensive　Stuff	Makes　expensive,　high-level　items　available.	1	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
10	3	2	2	0	30	Cheaper　Stuff	Makes　cheaper,　low-level　items　available.	2	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
50	3	3	2	0	30	Stronger　Enemies	Makes　enemies　stronger.	3	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
200	3	9	2	0	30	Even　Stronger　Enemies	Makes　enemies　much　more　stronger.	9	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
1000	3	12	2	0	30	Strongest　Enemies	Makes　enemies　become　the　strongest　ever.	12	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
50	3	4	2	0	30	Weaker　Enemies	Makes　enemies　weaker.	4	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
200	3	10	2	0	30	Even　Weaker　Enemies	Makes　enemies　much　more　weaker.	10	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
1000	3	13	2	0	30	Weakest　Enemies	Makes　enemies　become　the　weakest　ever.	13	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
500	3	5	3	0	0	Eyewear　Inventory	Adds　variety　to　the　shop's　selection.	5	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
500	3	6	3	0	0	Belt　Inventory	Adds　variety　to　the　shop's　selection.	6	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
500	3	7	3	0	0	Shoe　Inventory	Adds　variety　to　the　shop's　selection.	7	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
500	3	8	3	0	0	Robe　Inventory	Adds　variety　to　the　shop's　selection.	8	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
1000	5	11	3	1	0	Base　Map　Workshop	Pursue　the　art　of　Prinny　explosion!	11	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
500	15	20	4	1	-20	Improve　Counterattack	Increases　number　of　character's　counterattacks.	20	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
500	15	21	4	1	-40	Improve　Movement	Enables　character　to　move　farther.	21	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
1000	15	22	4	1	-50	Increase　Magichange　Turns	Gain　more　turns　for　Magichange.	22	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
100	8	80	2	0	0	Make　a　Tough　Guy	Allows　you　to　create　a　Tough　Guy.	80	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
100	8	81	2	0	0	Make　a　Bad　Guy	Allows　you　to　create　a　Bad　Guy.	81	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
100	8	82	2	0	0	Make　a　Trainer	Allows　you　to　create　a　Trainer.	82	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
100	8	83	2	0	0	Make　a　Strange　Old　Man	Allows　you　to　create　a　Strange　Old　Man.	83	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
100	8	84	2	0	0	Make　a　Busty　Beauty	Allows　you　to　create　a　Busty　Beauty.	84	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
100	12	85	4	0	0	Make　a　Ninja	Allows　you　to　create　a　Ninja.	85	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
100	12	86	4	0	0	Make　a　Kunoichi	Allows　you　to　create　a　Female　Ninja.	86	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
100	12	87	4	0	0	Make　a　Sunset　Type　of　Guy	Allows　you　to　create　a　sunset　kind　of　guy.	87	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
100	12	88	4	0	0	Make　a　Eastern　Swordsman	Allows　you　to　create　a　Swordsman　from　the　Far　East.	88	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
100	12	89	4	0	0	Make　a　Lovely　Hunter	Allows　you　to　create　a　Lovely　Hunter.	89	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
500	12	90	4	0	-40	Make　the　Ultimate　Warrior	Allows　you　to　create　the　Ultimate　Warrior.	90	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
200	5	57	3	0	0	Triple　EXP	Triples　the　EXP　for　the　1st　enemy　killed　(1　time).	57	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
200	5	58	3	0	0	Bonus　Gauge　Boost	Boosts　Bonus　Gauge　at　the　start　of　battle　(1　time).	58	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
200	5	59	3	0	0	Prinny　Day	Only　Prinnies　are　allowed　on　the　next　map.　But...	59	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
500	3	60	3	0	-30	Raise　Military　Funds	Extorts　money　from　the　senators.	60	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
7000	0	101	5	1	-100	Cave　of　Ordeals	Opens　the　gate　to　the　Cave　of　Ordeals.	101	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
9999	0	102	5	1	-100	Unleash　the　Land　of　Carnage	The　Shura　show　their　true　power.　(Sealed　Each　Cycle)	102	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
100	14	111	5	1	-10	Fight　a　Hero　of　Another　Game!	Battle　a　character　from　a　newly(?)　released　game.	111	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
200	16	112	5	1	-10	Return　of　the　Prism　Rangers	The　emotional　come　back　of　the　Prism　Rangers!!	112	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
400	22	115	5	1	-10	I　want　to　see　the　ending	You　can　watch　THE　ending　to　that　once　again?	115	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
500	24	116	5	1	-30	Unleash　the　Fallen　Maiden!	Challenges　a　battle　against　the　Fallen　Maiden.	116	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
600	26	117	5	1	-30	I　want　to　fight　an　Overlord!	Battle　an　Overlord　of　another　Netherworld.	117	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
5000	28	118	5	1	-30	Pick　a　fight　with　an　Overlord!	Picks　a　fight　with　an　Overlord.	118	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
1500	32	120	5	1	-50	Break　the　mysterious　Seal!	Breaks　the　mysterious　Seal.	120	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
3000	34	121	5	1	-50	Meet　the　strongest　Overlord!	Allows　a　viewing　with　the　strongest　Overlord.	121	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
9999	36	122	5	1	-100	Meet　Baal　the　Tyrant!	???????????????	122	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
9999	36	123	5	1	-100	Hardcore　Inst.:　Champloo	Receive　guidance　from　Mr.　Champloo.	123	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
9999	36	124	5	1	-100	Hardcore　Inst.:　Raspberyl	Take　Raspberyl's　passionate　lecture　on　love.	124	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
9999	36	125	5	1	-100	Hardcore　Inst.:　Mao	Mao　will　teach　you　the　truth　about　hardcore.	125	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
9999	36	126	5	1	-100	Face　the　Final　Battle!	?????????????	126	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
1000	10	201	5	1	-50	I　want　to　become　a　senator	You　will　run　for　a　senator　position.	201	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
20	15	203	5	1	-75	I　want　a　Stronger　Voice	You　will　have　a　stronger　voice　as　a　senator.	203	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
500	20	202	5	1	0	Attend　a　Dark　Assembly	Attend　a　Dark　Assembly　called　by　another　senator.	202	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
0	0	301	0	0	0	I　want　to　change　the　item　name	Allows　you　to　change　the　name　of　an　item.	301	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
50	6	311	3	0	-5	A　little　more　HP　growth	When　item　levels　up,　HP　value　increases　a　little.	311	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
50	6	312	3	0	-5	A　little　more　SP　growth	When　item　levels　up,　SP　value　increases　a　little.	312	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
50	6	313	3	0	-5	A　little　more　ATK　growth　	When　item　levels　up,　ATK　value　increases　a　little.	313	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
50	6	314	3	0	-5	A　little　more　DEF　growth	When　item　levels　up,　DEF　value　increases　a　little.	314	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
50	6	315	3	0	-5	A　little　more　INT　growth	When　item　levels　up,　INT　value　increases　a　little.	315	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
50	6	316	3	0	-5	A　little　more　RES　growth	When　item　levels　up,　RES　value　increases　a　little.	316	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
50	6	317	3	0	-5	A　little　more　HIT　growth	When　item　levels　up,　HIT　value　increases　a　little.	317	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
50	6	318	3	0	-5	A　little　more　SPD　growth	When　item　levels　up,　SPD　value　increases　a　little.	318	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
2000	12	321	4	0	-50	More　HP　growth	When　item　levels　up,　HP　value　increases.	321	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
2000	12	322	4	0	-50	More　SP　growth	When　item　levels　up,　SP　value　increases.	322	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
2000	12	323	4	0	-50	More　ATK　growth	When　item　levels　up,　ATK　value　increases.	323	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
2000	12	324	4	0	-50	More　DEF　growth	When　item　levels　up,　DEF　value　increases.	324	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
2000	12	325	4	0	-50	More　INT　growth	When　item　levels　up,　INT　value　increases.	325	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
2000	12	326	4	0	-50	More　RES　growth	When　item　levels　up,　RES　value　increases.	326	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
2000	12	327	4	0	-50	More　HIT　growth	When　item　levels　up,　HIT　value　increases.	327	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
2000	12	328	4	0	-50	More　SPD　growth	When　item　levels　up,　SPD　value　increases.	328	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
500	18	331	5	1	-30	Increase　Jump　Ability	The　jump　ability　for　the　item　will　increase.	331	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
500	18	332	5	1	-30	Increase　Movement　Ability	The　movement　range　for　the　item　will　increase.	332	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
500	18	333	5	1	-30	Increase　Attack　Range	The　attack　range　for　the　item　will　increase.	333	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
500	18	336	5	1	-30	Increase　Counter　Attacks	#　of　counter　attacks　for　the　item　will　increase.	336	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
500	18	334	5	1	-30	Increase　Critical　Rate	The　critical　rate　for　the　item　will　increase.	334	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
500	18	335	5	1	-30	Increase　POP　level	POP　of　the　item　will　increase.	335	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
